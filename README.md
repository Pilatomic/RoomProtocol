# RoomArduino

Use a recent version of Arduino IDE

Tested with version 1.8.5

Debian package for Ubuntu 16.04 (version 1.0.5) does'nt work

Load RoomArduino/RoomArduino.ino in Arduino IDE

Push software to Arduino

# RoomSoftware

Install dependencies ( for Ubuntu / Linux Mint ): 

```
$ apt install build-essential qt5-default qtbase5-dev qtdeclarative5-dev qtmultimedia5-dev libqt5serialport5-dev qtdeclarative5-models-plugin qml-module-qtquick-controls qml-module-qtquick-dialogs
```

Build RoomControlPanel :
```
$ cd RoomSoftware/controlpanel
$ qmake
$ make
$ ./RoomControlPanel
```

To use with remote host : compile and start RoomNode on host computer, and RoomControlPanel on local computer

On local computer : start RoomControlPanel with host address and port as argument : 
```
./RoomControlPanel XXX.XXX.XXX.XXX YYYYY
```
(Port can be omitted, default port will be used)

On host computer :
```
$ cd RoomSoftware/node
$ qmake
$ make
$ ./RoomNode YYYYY
```
with YYYYY the port to use (can be omitted, default port will be used)
