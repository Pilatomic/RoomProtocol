#ifndef ROOM_P_H
#define ROOM_P_H

#include <stdint.h>
#include "roomprotocol.h"


class RoomResource;

class RoomInternals
{
public:
    RoomInternals(const char* endpointName);
    void registerRes(RoomResource* res);
    void processSerial();

    void notifyResourceUpdated(RoomResource* res);
    void begin();

    void pushPayloadByte(uint8_t c);

    void sendLoggingMsg(uint8_t level, char* logMsg);

private:
    bool collectPayload(uint8_t c);
    void executePayload();

    void sendFrameResUpdate(RoomResource* res, uint8_t resultCode);
    void sendFrameDescriptorAndValues(uint8_t resultCode);
    void sendFrameError(uint8_t result);
    void sendFrameNack(RoomResource* res);
    void sendFrameAckAndValue(RoomResource* res);

    void pushPayloadFlashString(const char *string);

    RoomResource *getResFromId(uint8_t id);

    enum RxParserStage {
        Stage_Start = 0, Stage_Payload, Stage_Overflow
    };

    RxParserStage m_rxParserStage;
    bool m_isNextCharEscaped;
    char m_rxBuffer[ROOM_MAX_LENGTH_ARDUINO];
    uint8_t m_rxBufferIndex;

    const char* m_epName;

    RoomResource* m_firstRes;

    bool m_hasBegun;

    static const char s_roomMagicString[];
};


#endif // ROOM_P_H
