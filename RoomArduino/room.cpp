#include "room.h"
#include "room_p.h"

#include <Arduino.h>
#include <limits.h>

RoomLink::RoomLink(const char *endpointName):
    m_internals(RoomInternals(endpointName))
{

}

void RoomLink::processSerial(uint32_t delay)
{
    uint32_t startTime = millis();
    do {
        m_internals.processSerial();
    } while((millis() - startTime) < delay);
}

void RoomLink::begin()
{
    m_internals.begin();
}

void RoomLink::log(char *msg, uint8_t level)
{
    m_internals.sendLoggingMsg(level, msg);
}

//==============================================================================

RoomResource::RoomResource(RoomLink *link, const char* label, const Capabilities cap) :
    m_type(Type_None),
    m_internals(&link->m_internals),
    m_label(label),
    m_cap(cap),
    m_id(0)
{
    m_internals->registerRes(this);
}

bool RoomResource::canExec() const
{
    return m_cap & Cap_Exec;
}

bool RoomResource::canNotify() const
{
    return m_cap & Cap_Notif;
}

//==============================================================================



RoomProcedure::RoomProcedure(RoomLink *link, const char *label, RoomProcedure::methodCallback callback) :
    RoomResource(link, label, RoomResource::Cap_Exec)
{
    m_type = Type_None;
    m_callback = callback;
}

bool RoomProcedure::parseData(const char *data, uint8_t length)
{
    (void)data;
    (void)length;
    m_callback();
    return true;
}

void RoomProcedure::sendValue()
{
    return;
}

uint8_t RoomProcedure::getSize()
{
    return 0;
}


//==============================================================================

RoomBool::RoomBool(RoomLink* link, const char* label, const Capabilities cap, onBoolSetCallback callback) :
    RoomResource(link, label, cap),
    m_callback(callback)
{
    m_value = false;
    m_type = Type_Bool;
}

bool RoomBool::get()
{
    return m_value;
}

void RoomBool::set(bool value)
{
    m_value = value;
    m_internals->notifyResourceUpdated(this);
    if(m_callback) m_callback(m_value);
}

RoomBool &RoomBool::operator =(bool b)
{
    this->set(b);
    return *this;
}

RoomBool::operator bool()
{
    return m_value;
}

bool RoomBool::parseData(const char *data, uint8_t length)
{
    if(length != 1 ) return false;
    m_value = (data[0] != 0x00);
    if(m_callback) m_callback(m_value);
    return true;
}

void RoomBool::sendValue()
{
    m_internals->pushPayloadByte((m_value == true) ? 0x01 : 0x00);
}

uint8_t RoomBool::getSize()
{
    return 1;
}

//==============================================================================

RoomColor::RoomColor(RoomLink *link, const char *label, const RoomResource::Capabilities cap, onColorSetCallBack callback) :
    RoomResource(link, label, cap),
    m_red(0x00), m_green(0x00), m_blue(0x00),
    m_callback(callback)
{
    m_type = Type_Color;
}

uint8_t RoomColor::red()
{
    return m_red;
}

uint8_t RoomColor::green()
{
    return m_green;
}

uint8_t RoomColor::blue()
{
    return m_blue;
}

void RoomColor::set(uint8_t red, uint8_t green, uint8_t blue)
{
    m_red = red;
    m_green = green;
    m_blue = blue;
    m_internals->notifyResourceUpdated(this);
    if(m_callback) m_callback(m_red ,m_green, m_blue);
}

void RoomColor::setRed(uint8_t red)
{
    m_red = red;
    m_internals->notifyResourceUpdated(this);
    if(m_callback) m_callback(m_red ,m_green, m_blue);
}

void RoomColor::setGreen(uint8_t green)
{
    m_green = green;
    m_internals->notifyResourceUpdated(this);
    if(m_callback) m_callback(m_red ,m_green, m_blue);
}

void RoomColor::setBlue(uint8_t blue)
{
    m_blue = blue;
    m_internals->notifyResourceUpdated(this);
    if(m_callback) m_callback(m_red ,m_green, m_blue);
}

bool RoomColor::parseData(const char *data, uint8_t length)
{
    if(length != 3) return false;
    m_red   = data[0];
    m_green = data[1];
    m_blue  = data[2];
    if(m_callback) m_callback(m_red ,m_green, m_blue);
    return true;
}

void RoomColor::sendValue()
{
    m_internals->pushPayloadByte(m_red);
    m_internals->pushPayloadByte(m_green);
    m_internals->pushPayloadByte(m_blue);
}

uint8_t RoomColor::getSize()
{
    return 3;
}


//==============================================================================


RoomInteger::RoomInteger(RoomLink *link, const char *label, const RoomResource::Capabilities cap, RoomInteger::onIntegerSetCallback callback) :
    RoomResource(link, label, cap),
    m_value(0), m_min(INT32_MIN), m_max(INT32_MAX),
    m_callback(callback)
{
    m_type = Type_Integer;
}

RoomInteger &RoomInteger::operator =(int32_t i)
{
    set(i);
    return *this;
}

RoomInteger &RoomInteger::operator +=(int32_t i)
{
    set(m_value + i);
    return *this;
}

RoomInteger &RoomInteger::operator -=(int32_t i)
{
    set(m_value - i);
    return *this;
}

RoomInteger &RoomInteger::operator *=(int32_t i)
{
    set(m_value * i);
    return *this;
}

RoomInteger &RoomInteger::operator /=(int32_t i)
{
    set(m_value / i);
    return *this;
}

RoomInteger::operator int32_t()
{
    return m_value;
}

int32_t RoomInteger::get() const
{
    return m_value;
}


void RoomInteger::clampValueAndNotify(void)
{
   if(m_value > m_max)        m_value = m_max;
   else if(m_value < m_min )  m_value = m_min;
   m_internals->notifyResourceUpdated(this);
   if(m_callback) m_callback(m_value);
}


void RoomInteger::set(int32_t value)
{
    m_value = value;
    clampValueAndNotify();
}

void RoomInteger::setMin(int32_t min)
{
    m_min = min;
    clampValueAndNotify();
}

int32_t RoomInteger::getMin() const
{
    return m_min;
}

void RoomInteger::setMax(int32_t max)
{
    m_max = max;
    clampValueAndNotify();
}

int32_t RoomInteger::getMax() const
{
    return m_max;
}

bool RoomInteger::parseData(const char *data, uint8_t length)
{
    if(length != sizeof(m_value)) return false;

    uint32_t newValue = uint8_t(data[0]);
    for(uint8_t i = 1 ; i < sizeof(m_value) ; i++)
    {
        newValue = (newValue << 8) | uint8_t(data[i]);
    }
    m_value = int32_t(newValue);

    if(m_value > m_max)        m_value = m_max;
    else if(m_value < m_min )  m_value = m_min;

    if(m_callback) m_callback(m_value);
    return true;
}

void RoomInteger::sendValue()
{
    for(int8_t i = ((sizeof(m_value)-1) << 3) ; i >= 0 ; i-=8)
    {
        m_internals->pushPayloadByte( (m_value >> i) & 0xFF);
    }
}

uint8_t RoomInteger::getSize()
{
    return (uint8_t)sizeof(m_value);
}
