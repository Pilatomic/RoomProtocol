/*
Project:	Gourmandise
Board:
MCU:
FRENQUENCY:	8000000

Created using Qt Creator
*/
#ifndef ARDUINO_H
#define ARDUINO_H
#include <Arduino.h>
#endif

#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include "room.h"

const char name[] PROGMEM = "ROOM DEMO DEVICE";
const char flag1Str[] PROGMEM = "Booleen";
const char colorStr[] PROGMEM = "Couleur";
const char string1Str[] PROGMEM = "String";
const char ledStr[] PROGMEM = "LED";
const char intStr[] PROGMEM = "Counter";
const char funcStr[] PROGMEM = "Function";

#define LOOP_DURATION   10

void resetCounter();

RoomLink roomLink(name);

RoomBool flag1(&roomLink, flag1Str, RoomResource::Cap_NotifExec);
RoomColor color1(&roomLink, colorStr, RoomResource::Cap_NotifExec);
RoomString<20> string1(&roomLink, string1Str, RoomResource::Cap_NotifExec);
RoomBool led1(&roomLink, ledStr, RoomResource::Cap_NotifExec, &writeLed);
RoomInteger int1(&roomLink, intStr, RoomResource::Cap_NotifExec);
RoomProcedure func1(&roomLink, funcStr, &resetCounter);

void writeLed(bool ledValue)
{
    digitalWrite(13,ledValue ? HIGH : LOW);
}

void resetCounter()
{
    int1 = 0;
}


void setup() {
    wdt_enable(WDTO_4S);
    wdt_reset();

    Serial.begin(115200); // Initialize serial communications with the PC

    pinMode(13,OUTPUT);
    digitalWrite(13,LOW);

    color1.set(random(255),random(255),random(255));
    string1.fill( 65 + random(50) );

    int1.set(0x7FFFF000);

    roomLink.begin();

    wdt_reset();

}


void loop()
{
    flag1 = !flag1;
    int1 += LOOP_DURATION;

    roomLink.processSerial(LOOP_DURATION);

    roomLink.log("LOOPED");

    wdt_reset();
}
