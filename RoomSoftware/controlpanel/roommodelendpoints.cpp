#include "roommodelendpoints.h"
#include "roomitem.h"
#include "roomsystem.h"

#include <QtDebug>

RoomModelEndpoints::RoomModelEndpoints(RoomSystem *roomSystem) :
    RoomModelAbstract(roomSystem)
{

}

int RoomModelEndpoints::count() const
{
    return m_endpointCount;
}

int RoomModelEndpoints::stalledCount() const
{
    return m_stalledCount;
}

const RoomItem *RoomModelEndpoints::itemFromIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        RoomItem *item = static_cast<RoomItem*>(index.internalPointer());
        if (item) return item;
    }
    return nullptr;
}

QModelIndex RoomModelEndpoints::createIndexForItem(const RoomItem *item) const
{
    //qDebug() << "CREATEINDEX" << item->label();
    if(!item) return QModelIndex();

    // Item is a known device
    int itemIndex = m_endpointList.indexOf(item);
    if(itemIndex >= 0)
    {
        return createIndex(itemIndex, 0, const_cast<RoomItem*>(item));
    }

    // Parent of item is a known device
    itemIndex = m_endpointList.indexOf(item->parent());
    if(itemIndex >= 0)
    {
        return createIndex(item->row(), 0, const_cast<RoomItem*>(item));
    }

    return QModelIndex();
}

QModelIndex RoomModelEndpoints::index(int row, int column, const QModelIndex &parent) const
{
    if(!hasIndex(row, column, parent)) return QModelIndex();

    const RoomItem* item = parent.isValid() ?
                m_endpointList.at(parent.row())->child(row) :
                m_endpointList.at(row);
    return createIndexForItem(item);
}

QModelIndex RoomModelEndpoints::parent(const QModelIndex &child) const
{
    const RoomItem* childItem = itemFromIndex(child);

    //If no child or child is first depth item, no parent
    if(!childItem || m_endpointList.contains(childItem)) return QModelIndex();

    //Now item is either in the list, or not
    const RoomItem* parentItem = childItem->parent();
    return createIndexForItem(parentItem);
}

int RoomModelEndpoints::rowCount(const QModelIndex &parent) const
{
    const RoomItem* item = itemFromIndex(parent);
    return item ? item->childrenCount() : m_endpointList.count();
}

int RoomModelEndpoints::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 1;
}

void RoomModelEndpoints::itemUpdatePrepare(const RoomItem *device)
{
    m_deviceUpdating = device;
    removeItemChildren(m_deviceUpdating);
}

void RoomModelEndpoints::itemUpdateHalfWay(int nextChildrenCount)
{
    Q_UNUSED(nextChildrenCount);
}

void RoomModelEndpoints::itemUpdateCompleted()
{
    if(!m_deviceUpdating->isNode() && !m_endpointList.contains(m_deviceUpdating))
    {
        appendItemAndNotify(m_deviceUpdating);
    }
    m_deviceUpdating = nullptr;

    updateEndpointCount();
    updateStalledCount();
}


void RoomModelEndpoints::itemIsStalledChanged(const RoomItem *item)
{
    RoomModelAbstract::itemIsStalledChanged(item);
    if(m_endpointList.contains(item)) updateStalledCount();
}

void RoomModelEndpoints::removeItemChildren(const RoomItem *item)
{
    if(!item->isDevice()) return;

    removeItemAndNotify(item);

    // Explore all children (maybe test if endpoint is Node before for optimisation ?)
    for(int i = 0 ; i < item->childrenCount() ; i++)
    {
        removeItemChildren(item->child(i));
    }
}

void RoomModelEndpoints::updateEndpointCount()
{
    if(m_endpointList.count() != m_endpointCount)
    {
        m_endpointCount = m_endpointList.count();
        emit countChanged(m_endpointCount);
    }
}

void RoomModelEndpoints::updateStalledCount()
{
    int stalledCountNow = 0;
    foreach(const RoomItem* endpoint, m_endpointList)
    {
        if(endpoint->isStalled()) stalledCountNow++;
    }

    if(stalledCountNow != m_stalledCount)
    {
        m_stalledCount = stalledCountNow;
        emit stalledCountChanged(m_stalledCount);
    }
}

void RoomModelEndpoints::appendItemAndNotify(const RoomItem *item)
{
    int insertRow = m_endpointList.count();
    beginInsertRows(QModelIndex(), insertRow, insertRow);
    m_endpointList.append(item);
    endInsertRows();
}

void RoomModelEndpoints::removeItemAndNotify(const RoomItem *item)
{
    int endpointIndex = m_endpointList.indexOf(item);
    if(endpointIndex < 0) return;
    beginRemoveRows(QModelIndex(), endpointIndex, endpointIndex);
    m_endpointList.remove(endpointIndex);
    endRemoveRows();
}

