#include "logger.h"

#include <QSaveFile>
#include <QDateTime>
#include <QTextStream>
#include <QCoreApplication>
#include <QtDebug>

Logger::Logger(QObject *parent) :
    QAbstractListModel(parent),
    m_capacity(100000)
{
}

void Logger::clear()
{
    beginResetModel();
    m_logs.clear();
    endResetModel();

    qInfo() << "Logs cleared";
}

void Logger::save()
{
    QDateTime currTime = QDateTime::currentDateTime();
    QString timestamp = currTime.toString("yyyy-MM-dd_ddd_hh-mm-ss");
    QString filePath = QCoreApplication::applicationDirPath() + QString("/log_%1.txt");
    QSaveFile fileSaver(filePath.arg(timestamp));
    if(fileSaver.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream saveStream(&fileSaver);
        for(auto logLineIt = m_logs.constBegin() ;
            logLineIt != m_logs.constEnd() ;
            logLineIt++)
        {
            saveStream << (*logLineIt) << endl;
        }
        if(fileSaver.commit())
            qInfo() << "Saved logs to" << fileSaver.fileName();
        else
            qInfo() << "Could not save logs to" << fileSaver.fileName();
    }
}


int Logger::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : m_logs.count();
}

QVariant Logger::data(const QModelIndex &index, int role) const
{
    Q_UNUSED(role)
    if(index.parent().isValid() ||(index.column() != 0)||(index.row() >= m_logs.count()))
        return QVariant();

    return QVariant::fromValue(m_logs.at(index.row()));
}

void Logger::trimLogs()
{
    int rowsToRemove = m_logs.count() - m_capacity;
    if(rowsToRemove <= 0) return;

    beginRemoveRows(QModelIndex(),0,rowsToRemove-1);
    while(rowsToRemove > 0)
    {
        m_logs.removeFirst();
        rowsToRemove--;
    }
    endRemoveRows();
}

int Logger::capacity() const
{
    return m_capacity;
}

void Logger::setCapacity(int capacity)
{
    m_capacity = capacity;
    trimLogs();
}

void Logger::logMessage(QtMsgType type, const QMessageLogContext &context, const QString &message)
{
    QString logStringTimeStamped = QStringLiteral("[%1] %2").arg(QTime::currentTime().toString("hh:mm:ss.zzz"), message);

    int appendRow = m_logs.count();
    beginInsertRows(QModelIndex(),appendRow,appendRow);
    m_logs.append(logStringTimeStamped);
    endInsertRows();

    emit logAdded(appendRow);

    trimLogs();
}
