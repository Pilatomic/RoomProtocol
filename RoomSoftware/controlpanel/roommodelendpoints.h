#ifndef ROOMMODELENDPOINTS_H
#define ROOMMODELENDPOINTS_H

#include "roommodelabstract.h"

class RoomSystem;
class RoomItem;

class RoomModelEndpoints : public RoomModelAbstract
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(int stalledCount READ stalledCount NOTIFY stalledCountChanged)

public:
    RoomModelEndpoints(RoomSystem* roomSystem);

    int count() const;
    int stalledCount() const;

    QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;

    void itemUpdatePrepare(const RoomItem *endpoint) override;
    void itemUpdateHalfWay(int nextChildrenCount) override;
    void itemUpdateCompleted() override;

    void itemIsStalledChanged(const RoomItem *item) override;


signals:
    void countChanged(int count);
    void stalledCountChanged(int stalledCount);

protected:
    virtual const RoomItem *itemFromIndex(const QModelIndex &index) const override;
    virtual QModelIndex createIndexForItem(const RoomItem *item) const override;
    void appendItemAndNotify(const RoomItem* item);
    void removeItemAndNotify(const RoomItem* item);
    void removeItemChildren(const RoomItem* item);
    void updateEndpointCount();
    void updateStalledCount();


private:
    QVector<const RoomItem*> m_endpointList;
    const RoomItem* m_deviceUpdating = nullptr;
    int m_endpointCount = 0;
    int m_stalledCount = 0;
};

#endif // ROOMMODELENDPOINTS_H
