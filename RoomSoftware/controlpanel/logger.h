#ifndef LOGGER_H
#define LOGGER_H

#include <QAbstractListModel>


class Logger : public QAbstractListModel
{
    Q_OBJECT

public:
    Logger(QObject *parent = nullptr);

    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;

    int capacity() const;
    void setCapacity(int capacity);

public slots:
    void logMessage(QtMsgType type, const QMessageLogContext &context, const QString &message);
    void clear();
    void save();

signals:
    void logAdded(int row);

private:
    void trimLogs();

    QList<QString> m_logs;
    QList<QMetaObject::Connection> m_modelConnections;
    int m_capacity;
};

#endif // LOGGER_H
