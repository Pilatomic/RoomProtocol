#include "roommodelabstract.h"
#include "roomitem.h"
#include "roomsystem.h"

RoomModelAbstract::RoomModelAbstract(RoomSystem *roomSystem) :
    QAbstractItemModel(roomSystem)
{
    connect(roomSystem, &RoomSystem::itemUpdatePrepare,
            this, &RoomModelAbstract::itemUpdatePrepare);
    connect(roomSystem, &RoomSystem::itemUpdateHalfWay,
            this, &RoomModelAbstract::itemUpdateHalfWay);
    connect(roomSystem, &RoomSystem::itemUpdateCompleted,
            this, &RoomModelAbstract::itemUpdateCompleted);
    connect(roomSystem, &RoomSystem::itemLabelChanged,
            this, &RoomModelAbstract::itemLabelChanged);
    connect(roomSystem, &RoomSystem::itemValueChanged,
            this, &RoomModelAbstract::itemValueChanged);
    connect(roomSystem, &RoomSystem::itemIsStalledChanged,
            this, &RoomModelAbstract::itemIsStalledChanged);

    connect(this, &RoomModelAbstract::sendValue,
            roomSystem, &RoomSystem::sendValue);
    connect(this, &RoomModelAbstract::requestEndpointReset,
            roomSystem, &RoomSystem::requestEndpointReset);
}

QHash<int, QByteArray> RoomModelAbstract::roleNames() const
{
    return QHash<int, QByteArray>(
    {
                    { Qt::DisplayRole   , "label"               },
                    { ValueRole         , "value"               },
                    { TypeRole          , "type"                },
                    { CapsRole          , "caps"                },
                    { LengthRole        , "length"              },
                    { LocationRole      , "location"            },
                    { ResetRole         , "reset"               },
                    { isStalledRole     , "isStalled"           }
                });
}

QVariant RoomModelAbstract::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) return QVariant();

    const RoomItem* item = itemFromIndex(index);

    switch(role)
    {
    case Qt::DisplayRole:
        return QVariant::fromValue(item->label());
    case LocationRole:
    case ValueRole:
        return QVariant::fromValue(item->value());
    case TypeRole:
        return QVariant::fromValue(item->type());
    case CapsRole:
        return QVariant::fromValue(item->caps());
    case LengthRole:
        return QVariant::fromValue(int(item->maxLength()));
    case isStalledRole:
        return QVariant::fromValue(item->isStalled());
    default:
        return QVariant();
    }
}

bool RoomModelAbstract::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid()) return false;

    const RoomItem* item = itemFromIndex(index);

    if(item->isDevice())
    {
        if(role == ResetRole)
        {
            emit requestEndpointReset(item);
            return true;
        }
    }
    else if(role == ValueRole)
    {
        emit sendValue(item, value);
        return true;
    }
    return false;
}


void RoomModelAbstract::itemLabelChanged(const RoomItem *item)
{
    QModelIndex itemIndex = createIndexForItem(item);
    if(itemIndex.isValid())
        emit dataChanged(itemIndex, itemIndex, QVector<int>(Qt::DisplayRole));
}

void RoomModelAbstract::itemValueChanged(const RoomItem *item)
{
    QModelIndex resourceIndex = createIndexForItem(item);
    if(resourceIndex.isValid())
        emit dataChanged(resourceIndex, resourceIndex, QVector<int>({ValueRole}));
}

void RoomModelAbstract::itemIsStalledChanged(const RoomItem *item)
{
    QModelIndex resourceIndex = createIndexForItem(item);
    if(resourceIndex.isValid())
        emit dataChanged(resourceIndex, resourceIndex, QVector<int>({isStalledRole}));
}
