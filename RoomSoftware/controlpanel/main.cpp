#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QString>
#include <QtQml>

#include <QHostAddress>

#include <QCommandLineParser>

#include "roomclient.h"
#include "logger.h"
#include "roommodelcomplete.h"
#include "roommodelendpoints.h"

#include "roomscriptengine.h"

static Logger logger;

void loggerHandler(QtMsgType type, const QMessageLogContext &context, const QString &message) {
    logger.logMessage(type, context, message);
}


int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);

#ifdef QT_NO_DEBUG
    qInstallMessageHandler(loggerHandler);
    qInfo() << "Started";
#endif

    QCommandLineParser parser;
    parser.addPositionalArgument("Hosts"  , "Hosts addresses and optionnal ports.", "[host[:port] ...]");

    QCommandLineOption logLength(QStringList() << "L" << "log-length", "Set log length.", "log length", QString::number(100000));
    parser.addOption(logLength);

    QCommandLineOption remoteOnly(QStringList() << "r" << "remote", "Do not attempt to connect to local endpoints");
    parser.addOption(remoteOnly);

    QCommandLineOption simpleDisplay(QStringList() << "s" << "simple", "Display simple list of endpoints");
    parser.addOption(simpleDisplay);

    QCommandLineOption expectedEndpointCount(QStringList() << "e" << "endpoints", "Set expected endpoint count", "expected endpoints", QString::number(0));
    parser.addOption(expectedEndpointCount);

    QCommandLineOption scriptPathArg(QStringList() << "q" << "script", "Set script path", "script path", QString());
    parser.addOption(scriptPathArg);

    parser.process(a);

    QStringList hostsList = parser.positionalArguments();
    RoomClient client(hostsList);

    if(parser.isSet(logLength)) logger.setCapacity(parser.value(logLength).toInt());
    if(!parser.isSet(remoteOnly)) client.discoverLocalEndpoints(1);

    qmlRegisterUncreatableType<RoomItem>
            ("RoomItem"     , 1 , 0,    "RoomItem",     "This type in only usable to access enums");

    if(parser.isSet(scriptPathArg))
    {
        new RoomScriptEngine(client.getRoomSystem(), parser.value(scriptPathArg));
    }

    //Create QML application
    QQmlApplicationEngine engine;
    QQmlContext *ctxt = engine.rootContext();
    ctxt->setContextProperty("roomclient", &client);
    ctxt->setContextProperty("roommodeltree", new RoomModelComplete(client.getRoomSystem()));
    ctxt->setContextProperty("roommodelendpoints", new RoomModelEndpoints(client.getRoomSystem()));
    ctxt->setContextProperty("logger", &logger);
    ctxt->setContextProperty("forcesimplemodel", parser.isSet(simpleDisplay));


    ctxt->setContextProperty("expectedendpointcount", parser.value(expectedEndpointCount).toInt());
    engine.load(QUrl("qrc:/qml/main.qml"));

    //Failed to load QML ? EXIT
    if(engine.rootObjects().isEmpty()) return -1;

    return a.exec();
}
