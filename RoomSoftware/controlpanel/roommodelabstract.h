#ifndef ROOMMODELABSTRACT_H
#define ROOMMODELABSTRACT_H

#include <QAbstractItemModel>

class RoomItem;
class RoomSystem;

class RoomModelAbstract : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum ItemRoles
    {
        ValueRole = Qt::UserRole + 1,
        TypeRole,
        CapsRole,
        LengthRole,
        LocationRole,
        ResetRole,
        isStalledRole
    };

    explicit RoomModelAbstract(RoomSystem *roomSystem);
    virtual QHash<int, QByteArray> roleNames() const override;

    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role) override;

    virtual void itemUpdatePrepare(const RoomItem* endpoint) = 0;
    virtual void itemUpdateHalfWay(int nextChildrenCount) = 0;
    virtual void itemUpdateCompleted() = 0;
    virtual void itemLabelChanged(const RoomItem* item);
    virtual void itemValueChanged(const RoomItem* item);
    virtual void itemIsStalledChanged(const RoomItem* item);

signals :
    void sendValue(const RoomItem* resource, QVariant value);
    void requestEndpointReset(const RoomItem* endpoint);

protected:
    virtual const RoomItem *itemFromIndex(const QModelIndex &index) const = 0;
    virtual QModelIndex createIndexForItem(const RoomItem *item) const = 0;

};

#endif // ROOMMODELABSTRACT_H
