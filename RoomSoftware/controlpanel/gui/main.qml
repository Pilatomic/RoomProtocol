import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Layouts 1.0
import QtQml.Models 2.2
import RoomItem 1.0

Window {
    id: mainWindow
    objectName: "mainWindow"
    minimumWidth: 240
    minimumHeight: 320
    width: 320
    height: 480
    visible: true

    property var roommodel: (forcesimplemodel || !toggleModeButton.checked) ?
                                roommodelendpoints :
                                roommodeltree

    SplitView {
        anchors.fill: parent
        orientation: Qt.Vertical

        Item {
            Layout.minimumHeight:25
            Layout.maximumHeight:25
            Rectangle {
                property int availableEndpoints: roommodelendpoints.count - roommodelendpoints.stalledCount

                anchors.left: parent.left
                anchors.right: toggleModeButton.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                color: {
                    if(roommodelendpoints.count < expectedendpointcount || roommodelendpoints.stalledCount > 0 ) return "#FF4444";
                    if(roommodelendpoints.count > expectedendpointcount && expectedendpointcount > 0) return "#FFFF44";
                    return "#44FF44";
                }

                Text {
                    anchors.top:parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.right: parent.horizontalCenter
                    anchors.leftMargin: 10
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    text:   "Endpoints: " + parent.availableEndpoints +
                            ((expectedendpointcount > 0) ? (" / " + expectedendpointcount) : "")

                    font.bold: true
                }

                Text {
                    anchors.top:parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: parent.horizontalCenter
                    anchors.right:parent.right
                    anchors.leftMargin: 10
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    text: "Stalled : " + roommodelendpoints.stalledCount;

                    font.bold: true
                }
            }

            Button {
                id:toggleModeButton

                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                width: forcesimplemodel ? 0 : height

                text: "T"
                tooltip: "Toggle display mode"

                checkable: true

                visible: !forcesimplemodel
            }
        }



        TreeView {
            id: treeview

            Layout.minimumHeight:200
            Layout.fillHeight: true

            headerVisible: true

            model: roommodel

            selection: ItemSelectionModel {
                model: roommodel
            }

            TableViewColumn {
                id: labelColumn
                title: "Label"
                width: 150
                delegate: Component {
                    Text{
                        text: model ? model.label : ""
                        clip:true
                        color: (model && model.isStalled) ? "red" : "black"
                        //font.bold: model && model.isStalled
                        font.strikeout: model && model.isStalled
                        MouseArea{
                            anchors.fill: parent
                            onPressed: treeview.selection.setCurrentIndex(styleData.index, ItemSelectionModel.SelectCurrent);
                            onDoubleClicked: treeview.openDialog(model, styleData.index)
                        }
                    }
                }
            }

            TableViewColumn {
                id: capsColumn
                title: "Caps"
                role: "caps"
                width: 25
                delegate: Component {
                    MouseArea{
                        onPressed: treeview.selection.setCurrentIndex(styleData.index, ItemSelectionModel.SelectCurrent);
                        onDoubleClicked: treeview.openDialog(model, styleData.index)
                        Image{
                            source: "qrc:/icons/edit"
                            visible:switch(styleData.value * 1) {
                                    case RoomItem.Cap_Exec:
                                    case RoomItem.Cap_NotifExec:
                                        return true;
                                    default:
                                        return false;
                                    }
                            fillMode: Image.PreserveAspectFit
                            scale: 0.7
                        }
                    }
                }
            }

            TableViewColumn {
                id: valueColumn
                title: "Value"
                role: "value"
                width: 200
                delegate: Component {
                    Rectangle {
                        id: rect
                        color: "transparent"
                        ColorAnimation{
                            id: flashAnimation
                            from: "#FFFFFF00"
                            to: "#00FFFF00"
                            duration: 2000
                            target: rect
                            property: "color"
                            easing.type: Easing.InQuad
                        }

                        ResourceDelegate {
                            anchors.fill: parent
                            resType: model ? model.type : RoomItem.Type_None
                            resValue: model ? model.value : undefined
                            onResValueChanged: if(visible) flashAnimation.restart()
                            MouseArea{
                                anchors.fill: parent
                                onPressed: treeview.selection.setCurrentIndex(styleData.index, ItemSelectionModel.SelectCurrent);
                                onDoubleClicked: treeview.openDialog(model, styleData.index)
                            }
                        }
                        clip: true
                    }
                }
            }

            Connections { //Quick and dirty way to expand added items
                target: roommodel
                onRowsInserted: treeview.expand(roommodel.index(first, 0, parent))
            }

            function openDialog(model ,index)
            {
                if(!model) return;
                if((model.type * 1) === RoomItem.Type_Endpoint)
                    endpointDialog.openDialog(model)
                else if((model.type * 1) === RoomItem.Type_None && (model.caps * 1) === RoomItem.Cap_Exec)
                    procedureDialog.showEditorForModel(model, index)
                else
                    resourceDialog.showEditorForModel(model, index)
            }
        }

        Item {
            Layout.minimumHeight:80
            height: Layout.minimumHeight

            ScrollView {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: buttonsLayout.left

                ListView {
                    id: logView

                    model: logger

                    delegate: Text {
                        text: model.display
                        width: logView.width
                        wrapMode: Text.Wrap
                    }

                    clip : true

                    onModelChanged: {
                        currentIndex = -1
                    }

                    Connections {
                        target: logger
                        onLogAdded:{
                            if(logView.atYEnd) {
                                logView.currentIndex = row
                                logView.positionViewAtEnd()
                            }
                        }
                    }
                }
            }

            ColumnLayout {
                id: buttonsLayout

                width: 40

                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right

                spacing: 0

                Button {
                    id:clearButton
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    iconSource: "qrc:/icons/delete"
                    tooltip: "Clear"

                    onClicked: logger.clear()
                }

                Button {
                    id:saveButton
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    iconSource: "qrc:/icons/save"
                    tooltip: "Save"

                    onClicked: logger.save()
                }
            }
        }
    }

    DialogResource {
        id: resourceDialog
    }

    DialogEndpoint {
        id: endpointDialog
    }

    DialogProcedure {
        id: procedureDialog
    }
}
