import QtQuick 2.0
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import RoomItem 1.0

Dialog {
    visible: false
    title: "Procedure call"
    standardButtons:StandardButton.Apply | StandardButton.Cancel

    QtObject {
        id: p
        property var model
    }

    function showEditorForModel(model, index) {
        p.model = model
        fullResourceLabel.buildtext(model, index)
        width = colLayout.width
        height = colLayout.height
        open()
    }

    ColumnLayout{
        id: colLayout
        anchors.fill: parent
        spacing: 0

        Text {
            Layout.alignment: Qt.AlignHCenter
            text: "Confirm execute :"
        }

        Text {
            id: fullResourceLabel
            font.bold: true

            Layout.alignment: Qt.AlignHCenter

            function buildtext(model, index) {
                var toReturn = ""
                if(model) {
                    var parentLabel = roommodel.data(roommodel.parent(index), "")
                    if(parentLabel) toReturn += (parentLabel + " : ")
                    toReturn += model.label
                }
                text = toReturn
            }
        }
    }

    Keys.onEnterPressed: applyValue()


    onApply: applyValue()

    function applyValue()
    {
        p.model.value = undefined
        close()
    }
}
