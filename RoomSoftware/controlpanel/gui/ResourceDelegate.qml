import QtQuick 2.0
import RoomItem 1.0

Loader {
    id: delegateLoader

    property int resType
    property var resValue

    sourceComponent: {
        switch(resType) {
        case RoomItem.Type_Integer:
            return numberDelegate;
        case RoomItem.Type_String:
            return textDelegate;
        case RoomItem.Type_Bool:
            return boolDelegate;
        case RoomItem.Type_Color:
            return colorDelegate;
        default:
            return undefined;
        }
    }

    Component {
        id: boolDelegate
        Text {
            font.bold: true
            text: resValue ? "ON" : "OFF"
            color: resValue ? "green" : "red";
        }
    }

    Component {
        id: colorDelegate
        Row {
            spacing: 5
            Rectangle {
                id: indicator
                height: parent.height
                width: height
                radius: height / 2
                border.width: 2
                border.color: "black"
                color: resValue ? resValue : "transparent"
            }
            Text {
                id: name
                text: resValue ? resValue.toString().toUpperCase() : ""
                font.bold: true
            }
        }
    }

    Component {
        id: numberDelegate
        Text{
            text:resValue ? resValue.toString() : "0"
            font.bold: true
        }
    }

    Component {
        id: textDelegate
        Text{
            text:resValue ? resValue.toString() : ""
            font.bold: true
        }
    }
}
