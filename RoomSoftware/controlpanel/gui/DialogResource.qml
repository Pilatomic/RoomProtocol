import QtQuick 2.0
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import RoomItem 1.0

Dialog {
    visible: false
    title: "Resource Editor"
    standardButtons:StandardButton.Apply | StandardButton.Cancel

    QtObject {
        id: p
        property var model
    }

    function showEditorForModel(model, index) {
        if(! (model.caps & RoomItem.Cap_Exec)) return
        p.model = model
        currValueDelegate.setup(model)
        fullResourceLabel.buildtext(model, index)
        editor.setup(model)
        width = colLayout.width
        height = colLayout.height
        open()
    }

    ColumnLayout{
        id: colLayout
        anchors.fill: parent
        spacing: 0

        Text {
            Layout.alignment: Qt.AlignHCenter
            text: "Confirm value change for :"
        }

        Text {
            id: fullResourceLabel
            font.bold: true

            Layout.alignment: Qt.AlignHCenter

            function buildtext(model, index) {
                var toReturn = ""
                if(model) {
                    var parentLabel = roommodel.data(roommodel.parent(index), "")
                    if(parentLabel) toReturn += (parentLabel + " : ")
                    toReturn += model.label
                }
                text = toReturn
            }
        }

        ResourceDelegate {
            id: currValueDelegate

            Layout.alignment: Qt.AlignHCenter

            function setup(model) {
                resValue = Qt.binding(function() { return model.value} )
                resType = model.type
            }
        }

        Image{
            Layout.alignment: Qt.AlignHCenter
            source: "qrc:/icons/arrow_to"
        }

        ResourceEditor {
            id: editor
            Layout.alignment: Qt.AlignHCenter
        }
    }


    onAccepted: applyValue()
    onApply: applyValue()

    Keys.onEnterPressed: applyValue()

    function applyValue()
    {
        editor.active = false
        p.model.value = editor.resValue
        close()
    }
}
