import QtQuick 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import RoomItem 1.0


Loader {
    id: resourceEditor

    property var resValue
    property var resType

    function setup(model) {

        switch(model.type * 1) {
        case RoomItem.Type_Bool:
            sourceComponent = boolEdit;
            break;
        case RoomItem.Type_String:
            sourceComponent = stringEdit;
            break;
        case RoomItem.Type_Color:
            sourceComponent = colorEdit;
            break;
        case RoomItem.Type_Integer:
            sourceComponent = integerEdit;
            break;
        default:
            resValue= model.value
            console.error("EDITOR NOT IMPLEMENTED FOR TYPE")
            break;
        }

        if(source) active=true
        if(item && item.setup) item.setup(model)
    }

    Component {
        id: boolEdit
        ResourceDelegate {
            resType: RoomItem.Type_Bool
            resValue: resourceEditor.resValue
            function setup(model) {
                resourceEditor.resValue = !model.value
            }
        }
    }

    Component {
        id: colorEdit
        ColumnLayout {
            function setup(model) {
                resValue = model.value
                componentsEdit.itemAt(0).value = model.value.r * 255
                componentsEdit.itemAt(1).value = model.value.g * 255
                componentsEdit.itemAt(2).value = model.value.b * 255
            }

            function computeColor()
            {
                resValue = Qt.rgba(componentsEdit.itemAt(0).value / 255,
                                   componentsEdit.itemAt(1).value / 255,
                                   componentsEdit.itemAt(2).value / 255,
                                   1)
            }

            ResourceDelegate{
                resType: RoomItem.Type_Color
                resValue: resourceEditor.resValue
                Layout.alignment: Qt.AlignHCenter
            }

            Column {
                Layout.alignment: Qt.AlignHCenter

                Repeater
                {
                    id: componentsEdit
                    model: ["R", "G", "B"]
                    Row {
                        property int value
                        spacing: 5

                        Label {
                            text: modelData + ": "
                            width: 30
                        }
                        SpinBox {
                            id:spinVal
                            value: parent.value
                            minimumValue: 0
                            maximumValue: 255
                            stepSize: 1
                            onValueChanged:
                            {
                                parent.value = value
                                computeColor()
                            }
                        }
                        Slider {
                            id: slidVal
                            value: parent.value
                            minimumValue: 0
                            maximumValue: 255
                            stepSize: 1
                            width: 150
                            onValueChanged:
                            {
                                parent.value = value
                                computeColor()
                            }
                        }
                    }
                }
            }
        }
    }

    Component {
        id: integerEdit
        SpinBox {
            function setup(model)
            {
                resValue = model.value
                value = resValue
                focus = true
            }

            minimumValue: -2147483647
            maximumValue: 2147483647

            onValueChanged: resValue = value
        }
    }

    Component {
        id: stringEdit
        TextField {

            function setup(model)
            {
                resValue = model.value
                text = resValue
                focus = true
                maximumLength = model.length
            }
            onTextChanged: resValue = text
        }
    }
}
