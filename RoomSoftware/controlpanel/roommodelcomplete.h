#ifndef ROOMMODELCOMPLETE_H
#define ROOMMODELCOMPLETE_H

#include "roommodelabstract.h"

class RoomSystem;
class RoomItem;

class RoomModelComplete : public RoomModelAbstract
{
    Q_OBJECT
public:
    explicit RoomModelComplete(RoomSystem *roomSystem);

    QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;

public:
    void itemUpdatePrepare(const RoomItem* endpoint) override;
    void itemUpdateHalfWay(int nextChildrenCount) override;
    void itemUpdateCompleted() override;

protected:
    const RoomItem *itemFromIndex(const QModelIndex &index) const override;
    QModelIndex createIndexForItem(const RoomItem *item) const override;

private:
    const RoomItem* m_rootItem = nullptr;

    struct {
        QModelIndex index;
        int childrenCount = 0;
    } m_ongoingUpdate;

};

#endif // ROOMMODELCOMPLETE_H
