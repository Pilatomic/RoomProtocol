#include "roommodelcomplete.h"
#include "roomitem.h"
#include "roomsystem.h"

RoomModelComplete::RoomModelComplete(RoomSystem* roomSystem) :
    RoomModelAbstract (roomSystem),
    m_rootItem(roomSystem->rootItem())
{

}


QModelIndex RoomModelComplete::createIndexForItem(const RoomItem *item) const
{
    if(!item || item == m_rootItem) return QModelIndex();
    QModelIndex createdIndex = createIndex(item->row(), 0, const_cast<RoomItem*>(item));
    //qDebug() << "Created index" << createdIndex << "for item" << item->label();
    return createdIndex;
}


const RoomItem *RoomModelComplete::itemFromIndex(const QModelIndex &index) const
{
    if (index.isValid()) {
        RoomItem *item = static_cast<RoomItem*>(index.internalPointer());
        if (item) return item;
    }
    return m_rootItem;
}

QModelIndex RoomModelComplete::index(int row, int column, const QModelIndex &parent) const
{
    if(!hasIndex(row, column, parent)) return QModelIndex();
    RoomItem* childItem = itemFromIndex(parent)->child(row);
    return childItem ? createIndexForItem(childItem) : QModelIndex() ;
}

QModelIndex RoomModelComplete::parent(const QModelIndex &child) const
{
    if (!child.isValid()) return QModelIndex();
    const RoomItem* childItem = static_cast<RoomItem*>(child.internalPointer());
    const RoomItem* parentItem = childItem->parent();

//    qDebug() << parentItem->label() << "is parent of" << childItem->label();
    return (parentItem != m_rootItem) ? createIndexForItem(parentItem) : QModelIndex();

}

int RoomModelComplete::rowCount(const QModelIndex &parent) const
{
    if (parent.column() > 0) return 0;

    const RoomItem* item = itemFromIndex(parent);
//    qDebug() << "Item" << item->label() << "has" << item->childrenCount() << "rows";
    return item->childrenCount();
}

int RoomModelComplete::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 1;
}

void RoomModelComplete::itemUpdatePrepare(const RoomItem *endpoint)
{
    m_ongoingUpdate.index = createIndexForItem(endpoint);
    m_ongoingUpdate.childrenCount = endpoint->childrenCount();

    if(m_ongoingUpdate.childrenCount > 0)
        beginRemoveRows(m_ongoingUpdate.index,
                        0,
                        m_ongoingUpdate.childrenCount - 1);
}

void RoomModelComplete::itemUpdateHalfWay(int nextChildrenCount)
{
    if(m_ongoingUpdate.childrenCount > 0) endRemoveRows();
    m_ongoingUpdate.childrenCount = nextChildrenCount;
    if(m_ongoingUpdate.childrenCount > 0)
        beginInsertRows(m_ongoingUpdate.index,
                        0,
                        m_ongoingUpdate.childrenCount - 1);
}

void RoomModelComplete::itemUpdateCompleted()
{
    if(m_ongoingUpdate.childrenCount > 0) endInsertRows();
    m_ongoingUpdate.index = QModelIndex();
}
