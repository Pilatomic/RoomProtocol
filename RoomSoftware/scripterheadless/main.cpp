#include <QCoreApplication>
#include <QString>

#include <QHostAddress>

#include <QCommandLineParser>

#include "roomclient.h"
#include "roomscriptengine.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QCommandLineParser parser;
    parser.addPositionalArgument("Hosts"  , "Hosts addresses and optionnal ports.", "[host[:port] ...]");

    QCommandLineOption scriptPathArg(QStringList() << "q" << "script", "Set script path", "script path", QString());
    parser.addOption(scriptPathArg);

    parser.process(a);
    if(!parser.isSet(scriptPathArg)) parser.showHelp(-1);

    QStringList hostsList = parser.positionalArguments();
    RoomClient client(hostsList);
    RoomScriptEngine scriptEngine(client.getRoomSystem(), parser.value(scriptPathArg));


    return a.exec();
}
