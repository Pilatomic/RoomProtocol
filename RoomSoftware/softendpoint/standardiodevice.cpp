#include "standardiodevice.h"
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>    // POSIX terminal control definitions
#include <sys/ioctl.h>

#include <QtDebug>

#define READ_INTERVAL   10
#define READ_SIZE       32

void noBufferNorBlockingForFile(int fd)
{
    int flags = fcntl(fd, F_GETFL, 0);
    fcntl(fd, F_SETFL, flags | O_NONBLOCK);
    //setvbuf(file, nullptr, _IONBF, 0);

   /* struct termios   options;    // Terminal options
    int              rc;         // Return value

    // Get the current options for the port
    if((rc = tcgetattr(fd, &options)) < 0){
        fprintf(stderr, "failed to get attr: %d, %s\n", fd, strerror(errno));
        exit(EXIT_FAILURE);
    }

    cfmakeraw(&options);

    // Set the new attributes
    if((rc = tcsetattr(fd, TCSANOW, &options)) < 0){
        fprintf(stderr, "failed to set attr: %d, %s\n", fd, strerror(errno));
        exit(EXIT_FAILURE);
    }*/
}

inline ssize_t readFromStdin(void *buf, size_t count)
{
    return read(STDIN_FILENO, buf, count);
}

inline ssize_t writeToStdout(const void *buf, size_t count)
{
    return write(STDOUT_FILENO, buf, count);
}

StandardIODevice::StandardIODevice(QObject *parent) : QIODevice (parent)
{
    m_checkTimer = new QTimer(this);
    m_checkTimer->setInterval(READ_INTERVAL);
    connect(m_checkTimer, &QTimer::timeout,
            this, &StandardIODevice::onTimerTick);
}


bool StandardIODevice::open(QIODevice::OpenMode mode)
{
    mode &= QIODevice::ReadWrite;
    if(mode & QIODevice::ReadOnly)
    {
        noBufferNorBlockingForFile(STDIN_FILENO);
    }
    if(mode & QIODevice::WriteOnly)
    {
        noBufferNorBlockingForFile(STDOUT_FILENO);
    }
    QIODevice::open(mode);
    m_inputBuffer.clear();
    m_checkTimer->start();
    return true;
}

void StandardIODevice::close()
{
    m_checkTimer->stop();
    m_inputBuffer.clear();
}


qint64 StandardIODevice::bytesAvailable() const
{
    return m_inputBuffer.size();
}


qint64 StandardIODevice::readData(char *data, qint64 maxlen)
{
    int len = m_inputBuffer.length();
    if(maxlen < len) len = int(maxlen);
    memcpy(data, m_inputBuffer.data(), len);
    m_inputBuffer.remove(0, len);
    return len;
}

qint64 StandardIODevice::writeData(const char *data, qint64 len)
{
    return writeToStdout(data, size_t(len));
}

void StandardIODevice::onTimerTick()
{
    int n;
    if (ioctl(0, FIONREAD, &n) == 0 && n > 0)
    {
        char readBuf[READ_SIZE];
        do
        {
            n = int(readFromStdin(readBuf, READ_SIZE));
            if(n > 0 )
            {
                m_inputBuffer.append(readBuf, n);
                continue;
            }
        }while(false);

        emit QIODevice::readyRead();
    }
}

