#ifndef STANDARDIODEVICE_H
#define STANDARDIODEVICE_H

#include <QIODevice>
#include <QTimer>

class StandardIODevice : public QIODevice
{
public:
    StandardIODevice(QObject *parent = nullptr);

    // QIODevice interface
public:
    bool open(OpenMode mode) override;
    void close() override;
    qint64 bytesAvailable() const override;

protected:
    qint64 readData(char *data, qint64 maxlen) override;
    qint64 writeData(const char *data, qint64 len) override;

private:
    void onTimerTick();
    QByteArray m_inputBuffer;
    QTimer* m_checkTimer;
};

#endif // STANDARDIODEVICE_H
