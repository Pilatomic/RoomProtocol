#include "roomsoftinternals.h"
#include "roomprotocol.h"
#include "roomsoftresource.h"
#include "standardiodevice.h"
#include <QtDebug>

RoomSoftInternals::RoomSoftInternals(const QString & name, QObject *parent) :
    QObject(parent),
    m_name(name)
{

}

RoomSoftResource* RoomSoftInternals::getResFromId(uint8_t id)
{

    for(RoomSoftResource* currRes = m_firstRes;
        currRes != nullptr;
        currRes = currRes->m_next)
    {
        if(currRes->m_id == id) return currRes;
    }
    return nullptr;
}


void RoomSoftInternals::begin()
{
    QIODevice *stdiodevice = new StandardIODevice(this);
    stdiodevice->open(QIODevice::ReadWrite);
    connect(&m_link, &RoomLink::msgReceived,
            this, &RoomSoftInternals::onMessageReceived);
    m_link.setDevice(stdiodevice);

    sendFrameDescriptorAndValues(ROOM_VERB_RESET_DONE);
    m_hasBegun = true;
    //qDebug() << "Begin";
}

void RoomSoftInternals::registerRes(RoomSoftResource *res)
{
    if(m_firstRes)
    {
        RoomSoftResource *currRes = m_firstRes;
        while(currRes->m_next) currRes = currRes->m_next;
        currRes->m_next = res;
        res->m_id = currRes->m_id + 1;
    }
    else
    {
        m_firstRes = res;
        res->m_id = 1;
    }
}

void RoomSoftInternals::notifyResourceUpdated(RoomSoftResource *res)
{
    if(m_hasBegun) sendFrameResUpdate(res, ROOM_VERB_VALUE_CHANGED);
}

void RoomSoftInternals::onMessageReceived(const RoomMessage & msg)
{
    if(msg.addressCount() != 0)
    {
        qWarning() << "Soft endpoint received complex frame";
        return;
    }

    //Errors
    if(!msg.isValid())
        sendFrameError(ROOM_VERB_ERR_EMPTY_FRAME);
    else if(msg.payload().length() >= ROOM_MAX_LENGTH_NODE)
        sendFrameError(ROOM_VERB_ERR_OVERFLOW);
    else if(msg.verb() != ROOM_VERB_READ && msg.verb() != ROOM_VERB_WRITE_EXEC)
        sendFrameError(ROOM_VERB_ERR_UNKNOWN_VERB);

    //Descriptor
    else if(msg.noun() == ROOM_DESCRIPTOR_ID)
    {
        if(msg.verb() == ROOM_VERB_READ)            sendFrameDescriptorAndValues(ROOM_VERB_ACK);
        else if(msg.verb() == ROOM_VERB_WRITE_EXEC) sendFrameError(ROOM_VERB_ERR_CANT_WRITE);
        else                                        sendFrameError(ROOM_VERB_ERR_UNKNOWN_VERB);
    }

    //Resources
    RoomSoftResource* res = getResFromId(msg.noun());
    if(!res)                                sendFrameError(ROOM_VERB_ERR_UNKNOWN_NOUN);

    else if(msg.verb() == ROOM_VERB_WRITE_EXEC)
    {
        if(!res->canExec())                 sendFrameError(ROOM_VERB_ERR_CANT_WRITE);
        else if(res->parseData(msg.args())) sendFrameAckAndValue(res);
        else                                sendFrameNack(res);
    }

    else if(msg.verb()  == ROOM_VERB_READ)
    {
        if(!res->canNotify())               sendFrameError(ROOM_VERB_ERR_CANT_READ);
        else                                sendFrameAckAndValue(res);
    }

    else                                    sendFrameError(ROOM_VERB_ERR_UNKNOWN_VERB);
}

void RoomSoftInternals::sendFrameDescriptorAndValues(uint8_t resultCode)
{
    //Build & transmit descriptor
    QByteArray desc(ROOM_MAGIC_STRING);
    desc.append(char(ROOM_BYTE_SEP));
    desc.append(m_name.toLatin1());

    for(RoomSoftResource *res = m_firstRes;
        res != nullptr ;
        res = res->m_next)
    {
        desc.append(char(ROOM_BYTE_SEP));
        desc.append(char(res->m_cap | res->m_type));
        desc.append(char(res->getSize()));
        desc.append(res->m_label.toLatin1());
    }

    m_link.transmitMsg(RoomMessage(resultCode, ROOM_DESCRIPTOR_ID, desc));

    // Transmit values
    const uint8_t valueResultCode = (resultCode == ROOM_VERB_RESET_DONE) ?
                ROOM_VERB_RESET_DONE : ROOM_VERB_DESC_REQ;
    for(RoomSoftResource* res = m_firstRes;
        res != nullptr;
        res = res->m_next)
    {
        sendFrameResUpdate(res, valueResultCode);
    }
}

void RoomSoftInternals::sendFrameResUpdate(RoomSoftResource *res, uint8_t resultCode)
{
    if( !(res->m_cap & RoomSoftResource::Cap_Notif)) return;
    RoomMessage m(resultCode, res->m_id, res->getData());
    m_link.transmitMsg(m);
}

void RoomSoftInternals::sendFrameError(uint8_t result)
{
    RoomMessage m(result);
    m_link.transmitMsg(m);
}

void RoomSoftInternals::sendFrameNack(RoomSoftResource *res)
{
    RoomMessage m(ROOM_VERB_NACK, res->m_id);
    m_link.transmitMsg(m);
}

void RoomSoftInternals::sendFrameAckAndValue(RoomSoftResource *res)
{
    RoomMessage m(ROOM_VERB_ACK, res->m_id, res->getData());
    m_link.transmitMsg(m);
}

