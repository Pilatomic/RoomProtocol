#ifndef ROOMSOFTINTERNALS_H
#define ROOMSOFTINTERNALS_H

#include <QObject>
#include <QVector>
#include "roomlink.h"

class RoomSoftResource;

class RoomSoftInternals : public QObject
{
    Q_OBJECT

public:
    RoomSoftInternals(const QString &name, QObject *parent = nullptr);

    void begin();
    void registerRes(RoomSoftResource* res);
    void notifyResourceUpdated(RoomSoftResource* res);

private:
    void onMessageReceived(const RoomMessage &msg);
    void sendFrameDescriptorAndValues(uint8_t resultCode);
    void sendFrameResUpdate(RoomSoftResource *res, uint8_t resultCode);
    void sendFrameError(uint8_t result);
    void sendFrameNack(RoomSoftResource *res);
    void sendFrameAckAndValue(RoomSoftResource *res);
    RoomSoftResource *getResFromId(uint8_t id);

    RoomLink m_link;
    QString m_name;
    RoomSoftResource* m_firstRes = nullptr;
    bool m_hasBegun = false;
};

#endif // ROOMSOFTINTERNALS_H
