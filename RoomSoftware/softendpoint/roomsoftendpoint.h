#ifndef ROOMSOFTENDPOINT_H
#define ROOMSOFTENDPOINT_H

#include <QObject>

class RoomSoftInternals;

class RoomSoftEndpoint : public QObject
{
    Q_OBJECT
    friend class RoomSoftResource;
public:
    RoomSoftEndpoint(const QString &name, QObject *parent = nullptr);
    void begin();

private:
    RoomSoftInternals* m_internals = nullptr;
};

#endif // ROOMSOFTENDPOINT_H
