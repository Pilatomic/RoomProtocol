#ifndef ROOMSOFTRESOURCE_H
#define ROOMSOFTRESOURCE_H

#include <QObject>
#include <QColor>
#include "roomprotocol.h"

class RoomSoftEndpoint;
class RoomSoftInternals;

class RoomSoftResource : public QObject
{
    Q_OBJECT
    friend class RoomSoftInternals;

public:
    enum Capabilities
    {
        Cap_None = 0x00,
        Cap_Notif = 0x80,
        Cap_Exec = 0x40,
        Cap_NotifExec = Cap_Notif | Cap_Exec
    };

protected:
    RoomSoftResource(RoomSoftEndpoint* endpoint, const QString & label, const Capabilities cap);

    virtual bool parseData(const QByteArray& data) = 0;
    virtual QByteArray getData() = 0;
    virtual uint8_t getSize() = 0;

    enum Type
    {
        Type_None = ROOM_DESC_TYPE_NONE,
        Type_Bool = ROOM_DESC_TYPE_BOOL,
        Type_String = ROOM_DESC_TYPE_STRING,
        Type_Integer = ROOM_DESC_TYPE_INTEGER,
        Type_Color = ROOM_DESC_TYPE_COLOR,
    };

    Type m_type;

    RoomSoftInternals* m_internals;

    bool canExec() const;
    bool canNotify() const;

private:
    QString m_label;
    const Capabilities m_cap;
    uint8_t m_id;
    RoomSoftResource* m_next = nullptr;
};

//==============================================================================

class RoomSoftProcedure : public RoomSoftResource
{
    Q_OBJECT
public:
    typedef void (*methodCallback)(void);

    RoomSoftProcedure(RoomSoftEndpoint* endpoint, const char* label) :
        RoomSoftProcedure(endpoint, QString(label)){}
    RoomSoftProcedure(RoomSoftEndpoint* endpoint, const QString & label);

    bool parseData(const QByteArray& data) override;
    QByteArray getData() override;
    uint8_t getSize() override;

signals:
    void called();
};


//==============================================================================


class RoomSoftBool : public RoomSoftResource
{
    Q_OBJECT
    Q_PROPERTY(bool value READ get WRITE set NOTIFY changed)
public:
    RoomSoftBool(RoomSoftEndpoint* link, const char* label, const Capabilities cap) :
        RoomSoftBool(link, QString(label), cap){}
    RoomSoftBool(RoomSoftEndpoint* link, const QString & label, const Capabilities cap);

    RoomSoftBool &operator =(bool b);
    operator bool() const;

    void set(bool b);
    bool get() const;

protected:
    bool parseData(const QByteArray& data) override;
    QByteArray getData() override;
    uint8_t getSize() override;

signals:
    void changed(bool value);

private:
    bool m_value;
};

//==============================================================================

class RoomSoftString : public RoomSoftResource
{
    Q_OBJECT
    Q_PROPERTY(QString value READ get WRITE set NOTIFY changed)

public:
    RoomSoftString(RoomSoftEndpoint* endpoint, const char* label, const Capabilities cap) :
        RoomSoftString(endpoint, QString(label), cap){}
    RoomSoftString(RoomSoftEndpoint* endpoint, const QString & label, const Capabilities cap);

    operator const QString &() const;
    RoomSoftString &operator =(const QString s);

    const QString &get() const;
    void set(const QString &s);

protected:
    bool parseData(const QByteArray& data) override;
    QByteArray getData() override;
    uint8_t getSize() override;

signals:
    void changed(QString s);

private:
    QString m_string;
};

//==============================================================================

class RoomSoftColor : public RoomSoftResource
{
    Q_OBJECT
    Q_PROPERTY(QColor value READ get WRITE set NOTIFY changed)

public:

    RoomSoftColor(RoomSoftEndpoint* endpoint, const char * label, const Capabilities cap) :
        RoomSoftColor(endpoint, QString(label), cap) {}
    RoomSoftColor(RoomSoftEndpoint* endpoint, const QString & label, const Capabilities cap);

    operator const QColor &() const;
    RoomSoftColor &operator =(const QColor &c);

    const QColor &get() const;
    void set(const QColor &c);


protected:
    bool parseData(const QByteArray& data) override;
    QByteArray getData() override;
    uint8_t getSize() override;

signals:
    void changed(QColor color);

private:
    QColor m_color;
};

//==============================================================================

class RoomSoftInteger : public RoomSoftResource
{
    Q_OBJECT
    Q_PROPERTY(int32_t value READ get WRITE set NOTIFY changed)

public:
    RoomSoftInteger(RoomSoftEndpoint* endpoint, const char* label, const Capabilities cap) :
        RoomSoftInteger(endpoint, QString(label), cap){}
    RoomSoftInteger(RoomSoftEndpoint* endpoint, const QString & label, const Capabilities cap);

    operator int32_t() const;
    RoomSoftInteger &operator =(int32_t i);
    RoomSoftInteger &operator +=(int32_t i);
    RoomSoftInteger &operator -=(int32_t i);
    RoomSoftInteger &operator *=(int32_t i);
    RoomSoftInteger &operator /=(int32_t i);

    int32_t get() const;
    void set(int32_t i);

    int32_t getMin() const;
    void setMin(int32_t getMin);
    int32_t getMax() const;
    void setMax(int32_t getMax);

    inline bool isAtMin() const
        { return m_value == m_min; }
    inline bool isAtMax() const
        { return m_value == m_max; }

protected:
    bool parseData(const QByteArray& data) override;
    QByteArray getData() override;
    uint8_t getSize() override;

signals:
    void changed(int i);

private:
    void clampValueAndNotify(void);
    int32_t m_value;
    int32_t m_min;
    int32_t m_max;
};

#endif // ROOMSOFTRESOURCE_H
