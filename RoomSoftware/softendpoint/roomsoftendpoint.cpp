#include "roomsoftendpoint.h"
#include "roomsoftinternals.h"

RoomSoftEndpoint::RoomSoftEndpoint(const QString & name, QObject *parent) :
    QObject(parent),
    m_internals(new RoomSoftInternals(name, this))
{
}

void RoomSoftEndpoint::begin()
{
    m_internals->begin();
}

