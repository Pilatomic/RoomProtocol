#include "roomsoftresource.h"
#include "roomsoftendpoint.h"
#include "roomsoftinternals.h"

RoomSoftResource::RoomSoftResource(RoomSoftEndpoint *endpoint, const QString &label, const RoomSoftResource::Capabilities cap)  :
    m_type(Type_None),
    m_internals(endpoint->m_internals),
    m_label(label),
    m_cap(cap),
    m_id(0)
{
    m_internals->registerRes(this);
}

bool RoomSoftResource::canExec() const
{
    return m_cap & Cap_Exec;
}

bool RoomSoftResource::canNotify() const
{
    return m_cap & Cap_Notif;
}

//==============================================================================

RoomSoftProcedure::RoomSoftProcedure(RoomSoftEndpoint *endpoint, const QString &label) :
    RoomSoftResource (endpoint, label, RoomSoftResource::Cap_Exec)
{
    m_type = Type_None;
}

bool RoomSoftProcedure::parseData(const QByteArray &data)
{
    Q_UNUSED(data)
    emit called();
    return true;
}

QByteArray RoomSoftProcedure::getData()
{
    return QByteArray();
}

uint8_t RoomSoftProcedure::getSize()
{
    return 0;
}

//==============================================================================

RoomSoftBool::RoomSoftBool(RoomSoftEndpoint *link, const QString & label, const RoomSoftResource::Capabilities cap) :
    RoomSoftResource(link, label, cap)
{
    m_value = false;
    m_type = Type_Bool;
}

RoomSoftBool &RoomSoftBool::operator =(bool b)
{
    set(b);
    return *this;
}

RoomSoftBool::operator bool() const
{
    return m_value;
}

void RoomSoftBool::set(bool b)
{
    m_value = b;
    m_internals->notifyResourceUpdated(this);
    emit changed(m_value);
}

bool RoomSoftBool::get() const
{
    return m_value;
}


bool RoomSoftBool::parseData(const QByteArray &data)
{
    if(data.length() != 1 ) return false;
    m_value = (data[0] != 0x00);
    emit changed(m_value);
    return true;
}

QByteArray RoomSoftBool::getData()
{
    QByteArray a;
    a.append((m_value == true) ? 0x01 : 0x00);
    return a;
}

uint8_t RoomSoftBool::getSize()
{
    return 1;
}

//==============================================================================

RoomSoftString::RoomSoftString(RoomSoftEndpoint *endpoint, const QString &label, const RoomSoftResource::Capabilities cap) :
    RoomSoftResource(endpoint, label, cap)
{
    m_type=Type_String;
}

RoomSoftString &RoomSoftString::operator =(const QString s)
{
    set(s);
    return *this;
}

const QString &RoomSoftString::get() const
{
    return m_string;
}

void RoomSoftString::set(const QString &s)
{
    m_string = s;
    m_internals->notifyResourceUpdated(this);
    emit changed(s);
}

bool RoomSoftString::parseData(const QByteArray &data)
{
    if(data.length() > ROOM_SOFTEP_STRING_LENGTH) return false;
    m_string = QString::fromLatin1(data);
    emit changed(m_string);
    return true;
}

QByteArray RoomSoftString::getData()
{
    QByteArray a = m_string.toLatin1();
    a.truncate(ROOM_SOFTEP_STRING_LENGTH-1);
    return a;
}

RoomSoftString::operator const QString &() const
{
    return m_string;
}

uint8_t RoomSoftString::getSize()
{
    return uint8_t(ROOM_SOFTEP_STRING_LENGTH);
}

//==============================================================================

RoomSoftColor::RoomSoftColor(RoomSoftEndpoint *endpoint, const QString &label, const RoomSoftResource::Capabilities cap) :
    RoomSoftResource(endpoint, label, cap)
{
    m_type = Type_Color;
    m_color = QColor(0,0,0);
}

RoomSoftColor::operator const QColor &() const
{
    return m_color;
}

RoomSoftColor &RoomSoftColor::operator =(const QColor &c)
{
    set(c);
    return *this;
}

const QColor &RoomSoftColor::get() const
{
    return m_color;
}

void RoomSoftColor::set(const QColor &c)
{
    m_color = c;
    m_internals->notifyResourceUpdated(this);
    emit changed(c);
}



bool RoomSoftColor::parseData(const QByteArray &data)
{
    if(data.length() != 3) return false;
    m_color = QColor(uint8_t(data.at(0)),
                     uint8_t(data.at(1)),
                     uint8_t(data.at(2)));
    emit changed(m_color);
    return  true;
}

QByteArray RoomSoftColor::getData()
{
    QByteArray a;
    a.append(char(m_color.red()));
    a.append(char(m_color.green()));
    a.append(char(m_color.blue()));
    return a;
}

uint8_t RoomSoftColor::getSize()
{
    return 3;
}

//==============================================================================


RoomSoftInteger::RoomSoftInteger(RoomSoftEndpoint* endpoint, const QString & label, const Capabilities cap) :
    RoomSoftResource(endpoint, label, cap),
    m_value(0), m_min(INT32_MIN), m_max(INT32_MAX)
{
    m_type = Type_Integer;
}

RoomSoftInteger::operator int32_t() const
{
    return m_value;
}
RoomSoftInteger &RoomSoftInteger::operator =(int32_t i)
{
    m_value = i;
    clampValueAndNotify();
    return *this;
}

RoomSoftInteger &RoomSoftInteger::operator +=(int32_t i)
{
    m_value += i;
    clampValueAndNotify();
    return *this;
}

RoomSoftInteger &RoomSoftInteger::operator -=(int32_t i)
{
    m_value -= i;
    clampValueAndNotify();
    return *this;
}

RoomSoftInteger &RoomSoftInteger::operator *=(int32_t i)
{
    m_value *= i;
    clampValueAndNotify();
    return *this;
}

RoomSoftInteger &RoomSoftInteger::operator /=(int32_t i)
{
    m_value /= i;
    clampValueAndNotify();
    return *this;
}

int32_t RoomSoftInteger::get() const
{
    return m_value;
}

void RoomSoftInteger::set(int32_t i)
{
    m_value = i;
    clampValueAndNotify();
}

void RoomSoftInteger::clampValueAndNotify(void)
{
   if(m_value > m_max)        m_value = m_max;
   else if(m_value < m_min )  m_value = m_min;
   m_internals->notifyResourceUpdated(this);
   emit changed(m_value);
}

void RoomSoftInteger::setMin(int32_t min)
{
    m_min = min;
    clampValueAndNotify();
}

int32_t RoomSoftInteger::getMin() const
{
    return m_min;
}

void RoomSoftInteger::setMax(int32_t max)
{
    m_max = max;
    clampValueAndNotify();
}

int32_t RoomSoftInteger::getMax() const
{
    return m_max;
}

bool RoomSoftInteger::parseData(const QByteArray& data)
{
    if(data.length() != sizeof(m_value)) return false;

    uint32_t newValue = uint8_t(data.at(0));
    for(uint8_t i = 1 ; i < sizeof(m_value) ; i++)
    {
        newValue = (newValue << 8) | uint8_t(data.at(i));
    }
    m_value = int32_t(newValue);

    if(m_value > m_max)        m_value = m_max;
    else if(m_value < m_min )  m_value = m_min;

    return true;
}

QByteArray RoomSoftInteger::getData()
{
    QByteArray a;
    for(int8_t i = ((sizeof(m_value)-1) << 3) ; i >= 0 ; i-=8)
    {
        a.append(char((m_value >> i) & 0xFF));
    }
    return a;
}

uint8_t RoomSoftInteger::getSize()
{
    return uint8_t(sizeof(m_value));
}
