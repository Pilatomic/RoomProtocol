#include <QCoreApplication>

#include "roomsoftendpoint.h"
#include "roomsoftresource.h"

#include <QtDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    RoomSoftEndpoint s("MyEndpoint");
    RoomSoftProcedure p(&s,"MyProcdeure");
    RoomSoftBool b(&s, "MyBool", RoomSoftResource::Cap_NotifExec);
    RoomSoftString c(&s, "MyString", RoomSoftResource::Cap_NotifExec);
    RoomSoftColor col(&s, "MyColor", RoomSoftResource::Cap_NotifExec);
    RoomSoftInteger i(&s, "MyInteger", RoomSoftResource::Cap_NotifExec);

    QObject::connect(&p, &RoomSoftProcedure::called, [=]()
    {
        qDebug() << "CALLED";
    });
    s.begin();

    return a.exec();
}
