#include "serialendpoint.h"
#include "serialworker.h"
#include <QSerialPortInfo>
#include <QDebug>

#define WATCHDOG_INTERVAL   5000

QVector<AbstractEndpoint*> SerialEndpoint::discoverEndpoints()
{
    QList<QSerialPortInfo> portList = QSerialPortInfo::availablePorts();
    QVector<AbstractEndpoint*> endpoints;

    for(QList<QSerialPortInfo>::const_iterator it = portList.constBegin() ;
        it != portList.constEnd();
        it++)
    {
        endpoints.append(new SerialEndpoint(it->portName()));
    }

    return endpoints;
}

SerialEndpoint::SerialEndpoint(QString portName, QObject *parent) :
    AbstractEndpoint(parent),
    m_serialPortName(portName),
    m_worker(nullptr),
    m_thread(nullptr),
    m_threadWatchdogTimer(new QTimer(this))
{
    m_threadWatchdogTimer->setSingleShot(true);
    m_threadWatchdogTimer->setInterval(WATCHDOG_INTERVAL);
    connect(m_threadWatchdogTimer, &QTimer::timeout, this, &SerialEndpoint::onWatchdogTimeout);
}

SerialEndpoint::~SerialEndpoint()
{
    if(m_thread.isNull()) return;
    m_thread->quit();
    if(!m_thread->wait(3000))   //Wait until it actually has terminated (max. 3 sec)
    {
        m_thread->terminate();  //Thread didn't exit in time, probably deadlocked, terminate it!
        m_thread->wait();       //We have to wait again here!
    }
}

void SerialEndpoint::open()
{
    //qDebug() << "Serial Endpoint OPEN called";
    close();
    m_worker = new SerialWorker(m_serialPortName);
    m_thread = new QThread(this);
    m_worker->moveToThread(m_thread);
    connect(m_worker.data(), &SerialWorker::frameReceived,
            this, &SerialEndpoint::frameReceived,Qt::QueuedConnection);
    connect(m_thread.data(), &QThread::started,
            m_worker.data(), &SerialWorker::open );
    connect(m_thread.data(), &QThread::finished,
            m_worker.data(), &SerialWorker::close );
    connect(m_thread.data(), &QThread::finished,
            m_thread.data(), &QThread::deleteLater );
    connect(m_thread.data(), &QThread::finished,
            m_worker.data(), &SerialWorker::deleteLater );
    connect(m_worker.data(), &SerialWorker::openChanged,
            this, &SerialEndpoint::onWorkerOpenChanged);
    connect(m_worker.data(), &SerialWorker::pongReceived,
            this, &SerialEndpoint::onWorkerPongReceived);
    m_thread->start();
    m_threadWatchdogTimer->start();
}

void SerialEndpoint::close()
{
    if(m_thread.isNull()) return;
    m_thread->quit();
    m_threadWatchdogTimer->stop();
    setStatus(ES_Closed);
}

void SerialEndpoint::reset()
{
    if(m_thread.isNull()) return;
    QMetaObject::invokeMethod(m_worker, "reset", Qt::QueuedConnection);
}

void SerialEndpoint::transmitFrame(RoomMessage frame)
{
    if(frame.addressCount() != 0)
    {
        qWarning() << "Serial endpoint received complex frame";
        return;
    }

    if(m_thread.isNull()) return;
    if(!QMetaObject::invokeMethod(m_worker, "transmitFrame", Qt::QueuedConnection, Q_ARG(RoomMessage, frame)))
    {
        qDebug() << "Failed to invoke transmit method on serial thread";
    }
}

void SerialEndpoint::onWorkerOpenChanged(bool workerOpen)
{
    setStatus((m_worker && workerOpen) ? ES_Open : ES_Closed);
}

void SerialEndpoint::onWorkerPongReceived(bool received)
{
    if(received == true)
    {
        if(status() == ES_Stalled) setStatus(ES_Open);
        m_threadWatchdogTimer->start();
    }
}

void SerialEndpoint::onWatchdogTimeout()
{
    if(status() == ES_Open) setStatus(ES_Stalled);
}


QString SerialEndpoint::getUri() const
{
    return QStringLiteral("SER:")+m_serialPortName;
}
