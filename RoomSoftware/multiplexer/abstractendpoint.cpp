#include "abstractendpoint.h"

#include <QThread>

AbstractEndpoint::AbstractEndpoint(QObject *parent) :
    QObject(parent)
{

}

AbstractEndpoint::Status AbstractEndpoint::status()
{
    return m_status;
}


void AbstractEndpoint::setStatus(Status status)
{
    if(status != m_status)
    {
        m_status = status;
        emit statusChanged(m_status);
    }
}

bool AbstractEndpoint::operator==(const AbstractEndpoint &other) const
{
    return this->getUri() == other.getUri();
}
