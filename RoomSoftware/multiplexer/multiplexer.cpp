#include "multiplexer.h"
#include "serialendpoint.h"
#include "roomprotocol.h"
#include <QtDebug>

Multiplexer::Multiplexer(const QString & label,
                         QVector<AbstractEndpoint *> fixedEndpoints,
                         QObject *parent) :
    QObject(parent),
    m_fixedEndpoints(fixedEndpoints)
{
    m_nodeEndpoint = new NodeEndpoint(this, label);

    connect(m_nodeEndpoint, &AbstractEndpoint::frameReceived,
            this, &Multiplexer::onMsgReceived);

    //m_fixedEndpoints.append(new NodeEndpoint(this));
    foreach(AbstractEndpoint* endpoint, m_fixedEndpoints)
    {
        endpoint->setParent(this);
        connect(endpoint, &AbstractEndpoint::frameReceived,
                this, &Multiplexer::onMsgReceived);
        connect(endpoint, &AbstractEndpoint::statusChanged,
                this, &Multiplexer::updateStalledEndpoints);
        endpoint->open();
    }

    qDebug() << m_fixedEndpoints.count() << "fixed endpoints"; // Do not count NODE endpoint
}

bool Multiplexer::setAutoDiscovery(int interval)
{
    if(interval > 0)
    {
        if(!m_discoveryTimer)
        {
            m_discoveryTimer = new QTimer(this);
            connect(m_discoveryTimer, &QTimer::timeout,
                    this, &Multiplexer::doEndpointsDiscovery, Qt::UniqueConnection);
        }
        m_discoveryTimer->start(interval);
        return true;
    }
    else
    {
        delete m_discoveryTimer;
        m_discoveryTimer = nullptr;
        return false;
    }
}

/*
void Multiplexer::doEndpointsDiscovery()
{
    qDebug() << "Multiplexer : DoEndpointDiscovery";

    //TODO :  delete all endpoints managed but not available anymore
    //Currently we simply delete ALL endpoints
    while(!m_discoveredEndpoints.isEmpty())
    {
        AbstractEndpoint* currEndpoint = m_discoveredEndpoints.takeLast();
        currEndpoint->close();
        currEndpoint->deleteLater();
    }

    //Query all possible endpoints types
    m_discoveredEndpoints.append(SerialEndpoint::discoverEndpoints());

    //Then add available endpoints not managed yet
    for(int i = 0 ; i < m_discoveredEndpoints.size() ; i++)
    {
        AbstractEndpoint* currEndpoint = m_discoveredEndpoints.at(i);
        currEndpoint->setParent(this);
        connect(currEndpoint, &AbstractEndpoint::frameReceived, this, &Multiplexer::onMsgReceived);
        connect(currEndpoint, &AbstractEndpoint::statusChanged, this ,&Multiplexer::onEndpointStatusChanged);
        currEndpoint->open();
    }

    qDebug() <<  m_discoveredEndpoints.size() << "discovered endpoints";

    return;
}*/


void Multiplexer::doEndpointsDiscovery()
{
    //qDebug() << "Multiplexer : DoEndpointDiscovery";

    QVector<AbstractEndpoint*> availableEndpoints;
    availableEndpoints.reserve(m_discoveredEndpoints.size()); // Avoid multiple resize
    availableEndpoints.append(SerialEndpoint::discoverEndpoints());

    bool endpointsChanged = false;

    //First add new endpoints so they CAN NOT OVERLAP PREVIOUS ENDPOINT ID
    for(int i = 0 ; i < availableEndpoints.length() ; i++)
    {
        AbstractEndpoint* currEndpoint = availableEndpoints.at(i);
        currEndpoint->setObjectName(currEndpoint->getUri());
        bool isNew = (locateEndpointWithSameUri(m_discoveredEndpoints, currEndpoint) < 0);
        if(isNew)
        {
            int firstFreeIndex = m_discoveredEndpoints.indexOf(nullptr);
            if(firstFreeIndex < 0 ) m_discoveredEndpoints.append(currEndpoint);
            else m_discoveredEndpoints[firstFreeIndex] = currEndpoint;

            currEndpoint->setParent(this);
            connect(currEndpoint, &AbstractEndpoint::frameReceived, this, &Multiplexer::onMsgReceived);
            connect(currEndpoint, &AbstractEndpoint::statusChanged, this ,&Multiplexer::updateStalledEndpoints);
            currEndpoint->open();
            endpointsChanged = true;
        }
    }

    // Remove any endpoint which are not available any more
    for(int i = 0 ; i < m_discoveredEndpoints.length() ; i++)
    {
        AbstractEndpoint* currEndpoint = m_discoveredEndpoints.at(i);
        bool isGone = (locateEndpointWithSameUri(availableEndpoints, currEndpoint) < 0);
        if(isGone)
        {
            currEndpoint->close();
            currEndpoint->deleteLater();
            m_discoveredEndpoints.removeAt(i--);
            endpointsChanged = true;
        }
    }

    // Delete available endpoints (except those we just added)
    for(auto i = availableEndpoints.begin() ;
        i != availableEndpoints.end();
        i++)
    {
        AbstractEndpoint *currEndpoint = *i;
        if(!m_discoveredEndpoints.contains(currEndpoint)) delete currEndpoint;
    }

    if(endpointsChanged)
    {
        emit discoveredEndpointsChanged();
        updateStalledEndpoints();
        doAllEndpointDescriptorRequest();
        qDebug() <<  m_discoveredEndpoints.size() << "discovered endpoints";

    }
}

void Multiplexer::doAllEndpointDescriptorRequest()
{
    foreach(AbstractEndpoint* endpoint, m_fixedEndpoints)
    {
        endpoint->transmitFrame(RoomMessage(ROOM_VERB_READ, ROOM_DESCRIPTOR_ID));
    }
    foreach(AbstractEndpoint* endpoint, m_discoveredEndpoints)
    {
        endpoint->transmitFrame(RoomMessage(ROOM_VERB_READ, ROOM_DESCRIPTOR_ID));
    }
}

QVector<int> Multiplexer::stalledEndpoints() const
{
    return m_stalledEndpointsIds;
}

bool Multiplexer::doEndpointReset(int endpointId)
{
    AbstractEndpoint* endpoint = getEndpointFromId(endpointId);
    if(!endpoint) return false;
    endpoint->reset();
    return true;
}

bool Multiplexer::doEndpointDescriptorRequest(int endpointId)
{
    AbstractEndpoint* endpoint = getEndpointFromId(endpointId);
    if(!endpoint) return false;
    endpoint->transmitFrame(RoomMessage(ROOM_VERB_READ, ROOM_DESCRIPTOR_ID));
    return true;
}

void Multiplexer::msgToEndpoint(RoomMessage msg)
{
    if(msg.hasAddress())
    {
        uint8_t addr = msg.popAddress();
        AbstractEndpoint* endpoint = getEndpointFromId(addr);
        if(endpoint)
        {
            //qDebug() << "Transmit message" << msg.bytes() << "to addr" << addr;
            endpoint->transmitFrame(msg);
        }
    }
    else
    {
        m_nodeEndpoint->transmitFrame(msg);
    }
}

void Multiplexer::updateStalledEndpoints()
{
    QVector<int> nowStalledEndpointsUris;

    for(int i = 0 ; i < m_fixedEndpoints.length() ; i++)
    {
        if(m_fixedEndpoints.at(i)->status() == AbstractEndpoint::ES_Stalled)
        {
            nowStalledEndpointsUris.append(i);
        }
    }

    for(int i = 0 ; i < m_discoveredEndpoints.length() ; i++)
    {
        if(m_discoveredEndpoints.at(i)->status() == AbstractEndpoint::ES_Stalled)
        {
            nowStalledEndpointsUris.append(i + m_fixedEndpoints.length());
        }
    }

    if(nowStalledEndpointsUris != m_stalledEndpointsIds)
    {
        m_stalledEndpointsIds = nowStalledEndpointsUris;
        emit stalledEndpointsChanged(m_stalledEndpointsIds);
    }
}

void Multiplexer::onMsgReceived(RoomMessage msg)
{
    AbstractEndpoint* senderEndpoint = qobject_cast<AbstractEndpoint*>(sender());

    //Senser is node endpoint
    if(senderEndpoint == m_nodeEndpoint);

    //Sender is fixed endpoint
    else if(m_fixedEndpoints.contains(senderEndpoint))
    {
        int senderId = m_fixedEndpoints.indexOf(senderEndpoint);
        msg.pushAddress(senderId);
    }

    //Sender is discovered endpoint
    else if(m_discoveredEndpoints.contains(senderEndpoint))
    {
        int senderId = m_discoveredEndpoints.indexOf(senderEndpoint) + m_fixedEndpoints.size();
        msg.pushAddress(senderId);
    }

    else
    {
        qCritical() << "Multiplexer : Discarded message from unknown source";
        return;
    }
    emit msgFromEndpoint(msg);
}

AbstractEndpoint *Multiplexer::getEndpointFromId(int id)
{
    if(id < m_fixedEndpoints.size()) return m_fixedEndpoints.at(id);
    id-= m_fixedEndpoints.size();
    if(id < m_discoveredEndpoints.size()) return m_discoveredEndpoints.at(id);
    return nullptr;
}

int Multiplexer::locateEndpointWithSameUri(const QVector<AbstractEndpoint *> &c, const AbstractEndpoint *e)
{
    for(int i = 0 ;  i != c.length() ; i++)
    {
        const AbstractEndpoint* currEndpoint = c.at(i);
        if((currEndpoint != nullptr ) && ((*currEndpoint) == (*e))) return i;
    }
    return -1;
}

QStringList Multiplexer::buildEndpointsUriList()
{
    QStringList r;

    for(auto it = m_fixedEndpoints.constBegin();
        it != m_fixedEndpoints.constEnd();
        it ++)
    {
        r.append((*it)->getUri());
    }
    for(auto it = m_discoveredEndpoints.constBegin();
        it != m_discoveredEndpoints.constEnd();
        it ++)
    {
        r.append((*it)->getUri());
    }
    return r;
}


