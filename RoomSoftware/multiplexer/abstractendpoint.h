#ifndef ABSTRACTENDPOINT_H
#define ABSTRACTENDPOINT_H

#include "roommessage.h"
#include <QObject>

class AbstractEndpoint : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Status status READ status NOTIFY statusChanged)

public:
    enum Status{ES_Closed, ES_Open, ES_Stalled};
    Q_ENUM(Status)

    explicit AbstractEndpoint(QObject *parent = nullptr);

    Status status();

    virtual QString getUri() const = 0;
    bool operator==(const AbstractEndpoint& other) const;

signals:
    void frameReceived(RoomMessage frame);
    void statusChanged(Status status);

public slots:
    virtual void open() = 0;
    virtual void close() = 0;
    virtual void reset() = 0;
    virtual void transmitFrame(RoomMessage frame) = 0;

protected:
    void setStatus(Status status);

private:
    Status m_status = ES_Closed;

};

#endif // ABSTRACTENDPOINT_H
