#include "serialworker.h"
#include "roomprotocol.h"

#include <QThread>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QProcess>
#include <QDebug>

#define RESET_DURATION_MS   10


SerialWorker::SerialWorker(QString portName) :
    m_portName(portName)
{
    //disableDtrTogglingOnOpenForPort(m_portName);
}

void SerialWorker::open()
{
    //qDebug() << "Serial Worker" << m_portName << "attempting open";
    close();
    m_serialPort = new QSerialPort(m_portName,this);
    m_serialPort->setBaudRate(115200);
    m_serialPort->setParity(QSerialPort::NoParity);
    m_serialPort->setFlowControl(QSerialPort::NoFlowControl);
    m_serialPort->setStopBits(QSerialPort::OneStop);
    if(m_serialPort->open(QIODevice::ReadWrite))
    {
        m_link = new RoomLink(this);
        connect(m_link,&RoomLink::msgReceived,
                this, &SerialWorker::frameReceived);
        connect(m_link,&RoomLink::pongReceived,
                this, &SerialWorker::pongReceived);
        m_link->setDevice(m_serialPort);
        m_link->setAutoPing(SERIAL_WORKER_DELAY_PING, SERIAL_WORKER_DELAY_PING / 2);
        emit openChanged(true);
        //qDebug() << "Serial Worker" << m_portName << "opened";
    }
    else
    {
        close();
    }
}

void SerialWorker::close()
{
    if(m_link)
    {
        delete m_link;
        m_link = nullptr;
    }
    if(m_serialPort)
    {
        emit openChanged(false);
        m_serialPort->close();
        delete m_serialPort;
        m_serialPort = nullptr;
    }
    //qDebug() << "Serial Worker" << m_portName << "closed";
}

void SerialWorker::reset()
{
    if(m_serialPort)
    {
        m_serialPort->setDataTerminalReady(false);
        thread()->msleep(RESET_DURATION_MS);
        m_serialPort->setDataTerminalReady(true);
    }
}

void SerialWorker::transmitFrame(RoomMessage msg)
{
    if(m_link)
    {
        m_link->transmitMsg(msg);
        //qDebug() << "Serial" << m_portName << "transmit" << msg.bytes();
    }
    else
    {
        //qDebug() << "Serial" << m_portName << "FAILED TO transmit" << msg.bytes();
    }
}

void SerialWorker::disableDtrTogglingOnOpenForPort(QString portName)
{
    QString ttyPath = QSerialPortInfo(portName).systemLocation();
    QString sttyCmd = QStringLiteral("stty -F %1 -hupcl").arg(ttyPath);
    QProcess::execute(sttyCmd);
    qDebug() << "Disabled DTR toggling for port" << ttyPath;
}
