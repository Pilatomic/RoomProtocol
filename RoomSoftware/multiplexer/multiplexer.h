#ifndef MULTIPLEXER_H
#define MULTIPLEXER_H

#include "roommessage.h"
#include "nodeendpoint.h"
#include <QObject>
#include <QVector>

class AbstractEndpoint;

class Multiplexer : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVector<int> stalledEndpoints READ stalledEndpoints NOTIFY stalledEndpointsChanged)

public:
    explicit Multiplexer(const QString &label,
                         QVector<AbstractEndpoint*> fixedEndpoints = QVector<AbstractEndpoint*>(),
                         QObject *parent = nullptr);

    bool setAutoDiscovery(int interval);

public slots:
    void doEndpointsDiscovery();
    void doAllEndpointDescriptorRequest();
    QVector<int> stalledEndpoints() const;

    QStringList buildEndpointsUriList();

    bool doEndpointReset(int endpointId);
    bool doEndpointDescriptorRequest(int endpointId);

signals:
    void stalledEndpointsChanged(QVector<int> stalledEndpointsIds);
    void discoveredEndpointsChanged();

// <ACTUAL INTERFACE TO CONTROLPANEL>
public slots:
    void msgToEndpoint(RoomMessage msg);
signals:
    void msgFromEndpoint(RoomMessage msg);
// </ACTUAL INTERFACE TO CONTROLPANEL>

private slots:
    void updateStalledEndpoints();

protected:
    void onMsgReceived(RoomMessage msg);
    AbstractEndpoint *getEndpointFromId(int id);

    static int locateEndpointWithSameUri(const QVector<AbstractEndpoint*> & c, const AbstractEndpoint* e);

private:
    NodeEndpoint* m_nodeEndpoint = nullptr;
    const QVector<AbstractEndpoint*> m_fixedEndpoints;
    QVector<AbstractEndpoint*> m_discoveredEndpoints;
    QVector<int> m_stalledEndpointsIds;
    QTimer* m_discoveryTimer = nullptr;
};

#endif // MULTIPLEXER_H
