#ifndef NETWORKENDPOINT_H
#define NETWORKENDPOINT_H

#include "abstractendpoint.h"

#include <QTcpSocket>
#include <QObject>
#include <QTimer>

class RoomLink;

class NetworkEndpoint : public AbstractEndpoint
{
    Q_OBJECT
public:
    explicit NetworkEndpoint(QString hostName, quint16 port, QObject *parent = nullptr);

    QString getUri() const override;

public slots:
    void open() override;
    void close() override;
    void reset() override;
    void transmitFrame(RoomMessage frame) override;

private slots:
    void abort();
    void onSocketError(QAbstractSocket::SocketError);
    void onSocketConnected();

private:
    QTcpSocket *m_socket;
    RoomLink *m_link;
    QTimer* m_abortTimer;
    QTimer* m_retryTimer;
    QString m_hostName;
    quint16 m_port;

    void startConnection();
};

#endif // NETWORKENDPOINT_H
