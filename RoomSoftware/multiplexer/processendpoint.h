#ifndef PROCESSENDPOINT_H
#define PROCESSENDPOINT_H

#include "abstractendpoint.h"

#include <QObject>
#include <QProcess>

class RoomLink;

class ProcessEndpoint : public AbstractEndpoint
{
    Q_OBJECT
public:
    explicit ProcessEndpoint(const QString& path, const QString& args, QObject *parent = nullptr);

    QString getUri() const override;

public slots:
    void open() override;
    void close() override;
    void reset() override;
    void transmitFrame(RoomMessage frame) override;

private slots:
    void onProcessStateChanged(QProcess::ProcessState state);
    void onPongReceived(bool received);
    void onStandardErrorReadyRead();

private :
    QProcess *m_process;
    RoomLink *m_link;
};

#endif // PROCESSENDPOINT_H
