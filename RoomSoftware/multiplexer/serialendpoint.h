#ifndef SERIALENDPOINT_H
#define SERIALENDPOINT_H

#include "abstractendpoint.h"
#include <QThread>
#include <QPointer>
#include <QVector>
#include <QTimer>

class SerialWorker;

class SerialEndpoint : public AbstractEndpoint
{
    Q_OBJECT
public:

    SerialEndpoint(QString portName, QObject *parent = nullptr);
    ~SerialEndpoint() override;

    static QVector<AbstractEndpoint *> discoverEndpoints();


public slots:
    void open() override;
    void close() override;
    void reset() override;
    void transmitFrame(RoomMessage frame) override;

private slots:
    void onWorkerOpenChanged(bool workerOpen);
    void onWorkerPongReceived(bool received);
    void onWatchdogTimeout();

protected:
    QString getUri() const override;

private:
    const QString m_serialPortName;
    QPointer<SerialWorker> m_worker;
    QPointer<QThread> m_thread;
    QTimer* m_threadWatchdogTimer;
};

#endif // SERIALENDPOINT_H
