if (!contains( included_modules, $$PWD )) {
    included_modules += $$PWD
    message( "Including $$PWD" )

QT += core serialport network

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include($$PWD/../room/room.pri)

INCLUDEPATH += $$PWD

SOURCES += \
    $$PWD/multiplexer.cpp \
    $$PWD/abstractendpoint.cpp \
    $$PWD/nodeendpoint.cpp \
    $$PWD/serialendpoint.cpp \
    $$PWD/serialworker.cpp \
    $$PWD/networkendpoint.cpp\
    $$PWD/processendpoint.cpp

HEADERS += \
    $$PWD/multiplexer.h \
    $$PWD/abstractendpoint.h \
    $$PWD/nodeendpoint.h \
    $$PWD/serialendpoint.h \
    $$PWD/serialworker.h \
    $$PWD/networkendpoint.h \
    $$PWD/processendpoint.h

} else {
    message( "Skipping $$PWD: already included" )
}
