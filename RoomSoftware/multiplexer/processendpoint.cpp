#include "processendpoint.h"
#include "roomlink.h"
#include <QtDebug>

#define ARGS_SEPARATOR          ' '
#define PROCESS_PING_INTERVAL   500
#define RESTART_TIME            500

ProcessEndpoint::ProcessEndpoint(const QString &path, const QString &args, QObject *parent) :
    AbstractEndpoint (parent)
{
    QStringList argsList = args.split(ARGS_SEPARATOR);
    m_process = new QProcess(this);
    m_process->setProgram(path);
    m_process->setArguments(argsList);
    m_process->setReadChannelMode(QProcess::SeparateChannels);
    m_process->setReadChannel(QProcess::StandardOutput);
    connect(m_process, &QProcess::stateChanged,
            this, &ProcessEndpoint::onProcessStateChanged);
    connect(m_process, &QProcess::readyReadStandardError,
            this, &ProcessEndpoint::onStandardErrorReadyRead);

    m_link = new RoomLink(this);
    m_link->setDevice(m_process);
    connect(m_link, &RoomLink::msgReceived,
            this, &AbstractEndpoint::frameReceived);
    connect(m_link, &RoomLink::pongReceived,
            this, &ProcessEndpoint::onPongReceived);
}


QString ProcessEndpoint::getUri() const
{
    return QStringLiteral("PROC:%1").arg(m_process->program());
}

void ProcessEndpoint::open()
{
    m_process->start();
    m_link->setAutoPing(PROCESS_PING_INTERVAL, PROCESS_PING_INTERVAL / 2);
}

void ProcessEndpoint::close()
{
    m_process->terminate();
    m_link->setAutoPing(0,0);
    setStatus(ES_Closed);
}

void ProcessEndpoint::reset()
{
    m_process->terminate();
    QTimer::singleShot(RESTART_TIME,this, &ProcessEndpoint::open);
}

void ProcessEndpoint::transmitFrame(RoomMessage frame)
{
    m_link->transmitMsg(frame);
}

void ProcessEndpoint::onProcessStateChanged(QProcess::ProcessState state)
{
    if(status() == ES_Closed && state == QProcess::Running)
    {
        setStatus(ES_Open);
    }
}

void ProcessEndpoint::onPongReceived(bool received)
{
    if(received && status() == ES_Stalled)
    {
        setStatus(ES_Open);
    }
    else if(!received && status() != ES_Stalled)
    {
        setStatus(ES_Stalled);
    }
}

void ProcessEndpoint::onStandardErrorReadyRead()
{
    qWarning() << "StdErr from" << m_process->program() << m_process->readAllStandardError().simplified();
}
