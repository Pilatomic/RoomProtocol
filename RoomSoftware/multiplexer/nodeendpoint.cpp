#include "nodeendpoint.h"

#include "roomprotocol.h"
#include "multiplexer.h"

#include <QStringList>
#include <QtDebug>

NodeEndpoint::NodeEndpoint(Multiplexer *parent, const QString &label) :
    AbstractEndpoint(parent),
    m_mux(parent), m_label(label)
{
    connect(m_mux, &Multiplexer::stalledEndpointsChanged,
            this ,&NodeEndpoint::onStalledEndpointsChanged);
    connect(m_mux, &Multiplexer::discoveredEndpointsChanged,
            this, &NodeEndpoint::onDiscoveredEndpointsChanged);
    setStatus(ES_Open);
}

void NodeEndpoint::open()
{
    return;
}

void NodeEndpoint::close()
{
    return;
}

void NodeEndpoint::reset()
{
    return;
}

void NodeEndpoint::transmitFrame(RoomMessage frame)
{
    if(frame.addressCount() != 0)
    {
        qWarning() << "Node endpoint received complex frame";
        return;
    }

    QByteArray args = frame.args();
    uint8_t result = ROOM_VERB_ERR_UNKNOWN_NOUN;

    switch(frame.noun())
    {
    case ROOM_DESCRIPTOR_ID:
        result = processDescriptorRequest(&args) ? ROOM_VERB_ACK : ROOM_VERB_NACK;
        break;
    case ROOM_NODE_CMD_RESET:
        result = processResetRequest(&args) ? ROOM_VERB_ACK : ROOM_VERB_NACK;
        break;
    case ROOM_NODE_CMD_STALLED:
        args = stalledEndpointsToData(m_mux->stalledEndpoints());
        result = ROOM_VERB_ACK;
        break;
    default:
        args.clear();
        break;
    }

    emit AbstractEndpoint::frameReceived(RoomMessage(result, frame.noun(), args));
}

void NodeEndpoint::onStalledEndpointsChanged()
{
    emit AbstractEndpoint::frameReceived(RoomMessage(ROOM_VERB_VALUE_CHANGED,
                                                     ROOM_NODE_CMD_STALLED,
                                                     stalledEndpointsToData(m_mux->stalledEndpoints())));
}

void NodeEndpoint::onDiscoveredEndpointsChanged()
{
    emit AbstractEndpoint::frameReceived(RoomMessage(ROOM_VERB_VALUE_CHANGED,
                                                     ROOM_DESCRIPTOR_ID,
                                                     buildDescriptor(m_mux->buildEndpointsUriList())));
}

bool NodeEndpoint::processDescriptorRequest(QByteArray *args)
{
    QByteArray cmdArgs;
    cmdArgs.swap(*args);

    if(cmdArgs.length() != 0) return false;

    m_mux->doAllEndpointDescriptorRequest();
    QStringList endpointsUriList = m_mux->buildEndpointsUriList();
    (*args) = buildDescriptor(endpointsUriList);

    return true;
}

bool NodeEndpoint::processResetRequest(QByteArray *args)
{
    QByteArray cmdArgs;
    cmdArgs.swap(*args);

    if(cmdArgs.length() != 1) return false;

    return m_mux->doEndpointReset(cmdArgs.at(0));
}

QByteArray NodeEndpoint::stalledEndpointsToData(QVector<int> stalledEndpointsIds)
{
    QByteArray a;
    for(auto it = stalledEndpointsIds.constBegin() ;
        it != stalledEndpointsIds.constEnd() ;
        it ++)
    {
        a.append(char(*it));
    }
    return a;
}

QString NodeEndpoint::getUri() const
{
    return m_label;
}

QByteArray NodeEndpoint::buildDescriptor(QStringList uriList) const
{
    QByteArray a(ROOM_MAGIC_STRING);
    a.append(char(ROOM_BYTE_SEP));
    a.append(m_label.toLatin1());

    for(auto it = uriList.constBegin() ; it != uriList.constEnd() ; it++)
    {
        a.append(char(ROOM_BYTE_SEP));
        a.append(char(ROOM_DESC_TYPE_ENDPOINT));    //Endpoint / no capabilities
        a.append(char(0x00));                       //No length
        a.append(it->toLatin1());
    }

    return a;
}
