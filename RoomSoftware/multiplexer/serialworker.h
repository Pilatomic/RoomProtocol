#ifndef SERIALWORKER_H
#define SERIALWORKER_H

#include "roomlink.h"
#include "roommessage.h"
#include <QObject>

#define SERIAL_WORKER_DELAY_PING        1000

class QSerialPort;

class SerialWorker : public QObject
{
    Q_OBJECT
public:
    SerialWorker(QString portName);

    Q_INVOKABLE void open();
    Q_INVOKABLE void close();
    Q_INVOKABLE void reset();
    Q_INVOKABLE void transmitFrame(RoomMessage msg);

signals:
    void frameReceived(RoomMessage frame);
    void openChanged(bool open);
    void pongReceived(bool received);

private:
    static void disableDtrTogglingOnOpenForPort(QString portName);

    QString m_portName;
    QSerialPort* m_serialPort = nullptr;
    RoomLink* m_link = nullptr;
};


#endif // SERIALWORKER_H
