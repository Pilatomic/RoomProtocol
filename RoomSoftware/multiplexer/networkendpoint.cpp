#include "networkendpoint.h"
#include "roomlink.h"

#define ABORT_INTERVAL  2000
#define RETRY_INTERVAL  2000
#define PING_INTERVAL   2000
#define PING_TIMEOUT    1000

NetworkEndpoint::NetworkEndpoint(QString hostName, quint16 port, QObject *parent) :
    AbstractEndpoint(parent),
    m_hostName(hostName), m_port(port)
{
    m_socket = new QTcpSocket(this);
    connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(onSocketError(QAbstractSocket::SocketError)));
    connect(m_socket, &QTcpSocket::connected,
            this, &NetworkEndpoint::onSocketConnected);

    m_link = new RoomLink(this);
    connect(m_link, &RoomLink::msgReceived,
            this, &AbstractEndpoint::frameReceived);
    connect(m_link, &RoomLink::pongReceived, [=](bool received)
        {if(!received) this->abort();});

    m_abortTimer = new QTimer(this);
    m_abortTimer->setInterval(ABORT_INTERVAL);
    m_abortTimer->setSingleShot(true);
    connect(m_abortTimer, &QTimer::timeout,
            this, &NetworkEndpoint::abort);

    m_retryTimer = new QTimer(this);
    m_retryTimer->setInterval(RETRY_INTERVAL);
    m_retryTimer->setSingleShot(true);
    connect(m_retryTimer, &QTimer::timeout,
            this, &NetworkEndpoint::startConnection);

    m_link->setDevice(m_socket);
}

QString NetworkEndpoint::getUri() const
{
    return QStringLiteral("NET:%1:%2").arg(m_hostName).arg(m_port);
}

void NetworkEndpoint::open()
{
    startConnection();
}

void NetworkEndpoint::close()
{
    m_retryTimer->stop();
    m_abortTimer->stop();
    m_link->setAutoPing(0,0);
    m_socket->disconnectFromHost();
    setStatus(ES_Closed);
}

void NetworkEndpoint::reset()
{
}

void NetworkEndpoint::transmitFrame(RoomMessage frame)
{
    m_link->transmitMsg(frame);
}

void NetworkEndpoint::abort()
{
    m_retryTimer->start();
    m_abortTimer->stop();
    m_link->setAutoPing(0,0);
    m_socket->disconnectFromHost();
    setStatus(ES_Stalled);
}

void NetworkEndpoint::onSocketError(QAbstractSocket::SocketError)
{
    abort();
}

void NetworkEndpoint::onSocketConnected()
{
    m_abortTimer->stop();
    m_link->setAutoPing(2000,1000);
    setStatus(ES_Open);
    m_link->requestDescriptor();
}

void NetworkEndpoint::startConnection()
{
    m_retryTimer->stop();
    m_abortTimer->start();
    m_socket->abort();
    m_socket->connectToHost(m_hostName, m_port);
}


