#ifndef NODEENDPOINT_H
#define NODEENDPOINT_H

#include "abstractendpoint.h"
#include "roomlink.h"

#include <QObject>
#include <QMap>
#include <QString>

class Multiplexer;

class NodeEndpoint : public AbstractEndpoint
{
    Q_OBJECT
public:
    explicit NodeEndpoint(Multiplexer *parent, const QString & label);

public slots:
    void open() override;
    void close() override;
    void reset() override;
    void transmitFrame(RoomMessage frame) override;

private slots:
    void onStalledEndpointsChanged();
    void onDiscoveredEndpointsChanged();

protected:
    QString getUri() const override;
    QByteArray buildDescriptor(QStringList uriList) const;

    bool processDescriptorRequest(QByteArray *args);
    bool processResetRequest(QByteArray *args);

    QByteArray stalledEndpointsToData(QVector<int> stalledEndpointsIds);

    Multiplexer* m_mux;
    const QString m_label;
};

#endif // NODEENDPOINT_H
