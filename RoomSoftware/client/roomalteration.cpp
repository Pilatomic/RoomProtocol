#include "roomalteration.h"
#include "roomitem.h"


RoomAlteration::RoomAlteration(const RoomItem *item) :
    m_endpointName(item->parent()->label()),
    m_resourceLabel(item->label()),
    m_value(item->value())
{
}

RoomAlteration::RoomAlteration(const QString &endpoint, const QString &resource, const QVariant &value) :
    m_endpointName(endpoint),
    m_resourceLabel(resource),
    m_value(value)

{

}

QString RoomAlteration::endpointName() const
{
    return m_endpointName;
}

QString RoomAlteration::resourceLabel() const
{
    return m_resourceLabel;
}

QVariant RoomAlteration::value() const
{
    return m_value;
}

QString RoomAlteration::logString() const
{
    return QStringLiteral("%1/%2 changed to %3")
            .arg(m_endpointName)
            .arg(m_resourceLabel)
            .arg(m_value.toString());
}

bool RoomAlteration::isValid() const
{
    return !m_endpointName.isEmpty() && !m_resourceLabel.isEmpty() && m_value.isValid();
}

