#include "roomsystem.h"
#include "roomprotocol.h"
#include "roomitem.h"
#include <QDebug>


RoomSystem::RoomSystem(QObject *parent) :
    QObject(parent)
{
    m_rootItem = RoomItem::createRootItem();
}

RoomSystem::~RoomSystem()
{
    delete m_rootItem;
}

const RoomItem *RoomSystem::rootItem() const
{
    return m_rootItem;
}


const RoomItem *RoomSystem::updateEndpoint(QQueue<uint8_t> address, QString name, QVector<RoomItem*> resources)
{
    RoomItem* targetEndpoint = itemFromAddress(address);

    const int nextChildrenCount = resources.count();

    emit itemUpdatePrepare(targetEndpoint);
    targetEndpoint->clearChildrens();
    emit itemUpdateHalfWay(nextChildrenCount);
    targetEndpoint->updateChildrens(resources);
    emit itemUpdateCompleted();

    if(name != targetEndpoint->label())
    {
        targetEndpoint->setLabel(name);
        emit itemLabelChanged(targetEndpoint);
    }

    //Endpoint cant be stalled anymore, since it sent a descriptor
    if(targetEndpoint->isStalled())
    {
        targetEndpoint->setStalled(false);
        emit itemIsStalledChanged(targetEndpoint);
    }

    return targetEndpoint;
}

const RoomItem *RoomSystem::getItemAtAddress(QQueue<uint8_t> address)
{
    return itemFromAddress(address);
}

void RoomSystem::updateResourceValue(QQueue<uint8_t> address, uint8_t resourceId, QVariant value, bool isInitial)
{
    RoomItem* endpointItem = itemFromAddress(address);

    if(!endpointItem)
    {
        qWarning() << "Received resource value for non-existing endpoint";
        return;
    }
    RoomItem* resourceItem = endpointItem->child(resourceId);
    if(!resourceItem)
    {
        qWarning() << "Received value for non-existing resource";
        return;
    }

    resourceItem->setIsInitialvalue(isInitial);

    // If value is already set AND item is NOT a procedure
    /*if(resourceItem->value() == value && resourceItem->type() != RoomItem::Type_None)
    {
        qDebug() << "Value of" << resourceItem->label() << "already to" << value;
        return;
    }*/
    resourceItem->setValue(value);

    emit itemValueChanged(resourceItem);
}

void RoomSystem::updateStalledIds(QQueue<uint8_t> address, const QVector<uint8_t> stalledIds)
{
    RoomItem* endpointItem = itemFromAddress(address);
    for(uint8_t i = 0 ; i < endpointItem->childrenCount() ; i++)
    {
        RoomItem* childItem = endpointItem->child(i);
        bool isNowStalled = stalledIds.contains(i);
        updateStalledChildren(childItem, isNowStalled);
    }
}

void RoomSystem::updateStalledChildren(RoomItem *item, bool isStalled)
{
    if(item->isStalled() == isStalled) return;

    item->setStalled(isStalled);
    emit itemIsStalledChanged(item);
    qInfo() << item->label() << "is now"
            << (isStalled ? QString("STALLED") : QString("UNSTALLED"));

    if(!isStalled /*|| !item->isNode()*/) return;  //Do not remove STALLED flag from all children, wait for confirmation that children is alive

    for(int i = 0 ; i < item->childrenCount() ; i++)
    {
        updateStalledChildren(item->child(i), isStalled);
    }
}

RoomItem* RoomSystem::itemFromAddress(QQueue<uint8_t> address)
{
    RoomItem* item = m_rootItem;
    while(!address.isEmpty())
    {
        uint8_t index = address.dequeue();
        if(index > item->childrenCount()) return nullptr;
        item = item->child(index);
    }
    return  item;
}

