#ifndef ROOMTRANSLATOR_H
#define ROOMTRANSLATOR_H

#include "roommessage.h"
#include "roomitem.h"

#include <QObject>
#include <QQueue>

class RoomSystem;

class RoomTranslator : public QObject
{
    Q_OBJECT
public:
    explicit RoomTranslator(RoomSystem *model, QObject *parent = nullptr);
    Q_INVOKABLE void requestDescriptor(const QQueue<uint8_t> address = QQueue<uint8_t>()); // default is root endpoint
    Q_INVOKABLE void requestStalledEndpointsList(const QQueue<uint8_t> address = QQueue<uint8_t>()); // default is root endpoint

public slots:
    void msgFromEndpoint(RoomMessage msg);

signals:
    void msgToEndpoint(RoomMessage msg);

protected:
    QVariant argsToValue(RoomItem::Type type, QByteArray args);
    QVariant boolArgsToValue(QByteArray args);
    QVariant stringArgsToValue(QByteArray args);
    QVariant integerArgsToValue(QByteArray args);
    QVariant colorArgsToValue(QByteArray args);

    void resetEndpoint(const RoomItem *endpoint);
    bool sendValue(const RoomItem *resource, QVariant value);
    QByteArray boolValueToArgs(const QVariant & value);
    QByteArray stringValueToArgs(const QVariant & value);
    QByteArray colorValueToArgs(const QVariant & value);
    QByteArray integerValueToArgs(const QVariant & value);

    void processDescriptorMessage(QQueue<uint8_t> endpointAddr, const RoomMessage & descMsg);

    static void pushEndpointAddressToMsg(const RoomItem* endpoint, RoomMessage* msg);
    static void pushAddressToMsg(const QQueue<uint8_t> &address, RoomMessage* msg);
    static QQueue<uint8_t> extractAddressFromMessage(RoomMessage* msg);
    static QString printAddress(const QQueue<uint8_t> & address);

    static QVector<uint8_t> extractIdsFromByteArray(const QByteArray a);

    static inline bool isAcceptedVerbForDescriptor(uint8_t verb);
    static inline bool isAcceptedVerbForValue(uint8_t verb);


private:
    RoomSystem *m_model;
};

#endif // ROOMTRANSLATOR_H
