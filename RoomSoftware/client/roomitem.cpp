#include "roomitem.h"
#include <QtDebug>

RoomItem *RoomItem::createRootItem()
{
    RoomItem * rootItem = new RoomItem();
    rootItem->m_flags = Type_Endpoint;
    rootItem->m_value = QString("ROOT");
    return rootItem;
}

RoomItem *RoomItem::createResourceItem(uint8_t row, uint8_t flags, uint8_t length, const QString &label)
{
    RoomItem * resourceItem = new RoomItem();
    resourceItem->m_row = row;
    resourceItem->m_flags = flags;
    resourceItem->m_maxLength = length;

    if(resourceItem->isDevice()) resourceItem->m_value = QVariant::fromValue(label);
    else resourceItem->m_label=label;

    return resourceItem;
}


RoomItem::RoomItem()
{

}

RoomItem::~RoomItem()
{
    qDeleteAll(m_childrens);
}

RoomItem::Type RoomItem::type() const
{
    return Type(m_flags & ROOM_DESC_BITS_TYPE);
}

RoomItem::Capabilities RoomItem::caps() const
{
    return Capabilities(m_flags & ROOM_DESC_BITS_CAPS);
}

QString RoomItem::label() const
{
    return (isDevice() && m_label.isEmpty())
            ? QString("(%1)").arg(m_value.toString())
            : m_label;
}

void RoomItem::setLabel(const QString newLabel)
{
    m_label = newLabel;
}

uint8_t RoomItem::maxLength() const
{
    return m_maxLength;
}

const QVariant & RoomItem::value() const
{
    return m_value;
}

void RoomItem::setValue(QVariant value)
{
    m_value = value;
}

void RoomItem::setIsInitialvalue(bool b)
{
    m_isInitialValue = b;
}

bool RoomItem::isInitialValue() const
{
    return  m_isInitialValue;
}

RoomItem *RoomItem::parent() const
{
    return m_parent;
}

void RoomItem::setParent(RoomItem *parent)
{
    m_parent = parent;
}

RoomItem *RoomItem::child(int i) const
{
    return (i < 0 || i >= m_childrens.count()) ? nullptr : m_childrens.at(i);
}

int RoomItem::childrenCount() const
{
    return m_childrens.count();
}

bool RoomItem::isStalled() const
{
    return m_stalled;
}

void RoomItem::setStalled(bool stalled)
{
    m_stalled = stalled;
}

bool RoomItem::isDevice() const
{
    return type() == Type_Endpoint;
}

bool RoomItem::isNode() const
{
    return m_isNode;
}

void RoomItem::clearChildrens()
{
    qDeleteAll(m_childrens);
    m_childrens.clear();
}

bool RoomItem::updateChildrens(QVector<RoomItem *> childrens)
{
    if(!isDevice())
    {
        qWarning() << "Attempted to update non endpoint item";
        return false;
    }
    m_childrens.swap(childrens);
    qDeleteAll(childrens);

    m_isNode = isDevice();
    foreach(RoomItem* childItem, m_childrens)
    {
        childItem->setParent(this);
        if(m_isNode && !childItem->isDevice()) m_isNode = false;
    }

    //qDebug() << "Item" << m_label << "has now" << childrenCount() << "childrens";

    return true;
}


uint8_t RoomItem::row() const
{
    return m_row;
}
