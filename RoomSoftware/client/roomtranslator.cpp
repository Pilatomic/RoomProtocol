#include "roomtranslator.h"
#include "roommessage.h"
#include "roomprotocol.h"
#include "roomitem.h"
#include "roomsystem.h"

#include <QVariant>
#include <QColor>
#include <QDebug>

Q_DECLARE_METATYPE(QQueue<uint8_t>)

RoomTranslator::RoomTranslator(RoomSystem *model, QObject *parent) :
    QObject(parent),
    m_model(model)
{
    connect(m_model, &RoomSystem::sendValue,
            this, &RoomTranslator::sendValue);
    connect(m_model, &RoomSystem::requestEndpointReset,
            this, &RoomTranslator::resetEndpoint);
    qRegisterMetaType<QQueue<uint8_t>>();
}

void RoomTranslator::requestDescriptor(const QQueue<uint8_t> address)
{
    //qDebug() << "Request descriptor" << printAddress(address);
    QByteArray args;
    RoomMessage msg(ROOM_VERB_READ, ROOM_DESCRIPTOR_ID, args);
    pushAddressToMsg(address, &msg);
    emit msgToEndpoint(msg);
}

void RoomTranslator::requestStalledEndpointsList(const QQueue<uint8_t> address)
{
    //qDebug() << "Request stalled endpoints" << printAddress(address);
    QByteArray args;
    RoomMessage msg(ROOM_VERB_READ, ROOM_NODE_CMD_STALLED, args);
    pushAddressToMsg(address, &msg);
    emit msgToEndpoint(msg);
}

void RoomTranslator::resetEndpoint(const RoomItem *endpoint)
{
    if(endpoint->parent() == nullptr)
    {
        qWarning() << "Cannot reset root endpoint";
    }
    else
    {
        qInfo() << "Request reset of endpoint" << endpoint->label();
        QByteArray args;
        args.append(char(endpoint->row()));
        RoomMessage msg(ROOM_VERB_WRITE_EXEC, ROOM_NODE_CMD_RESET, args);
        pushEndpointAddressToMsg(endpoint->parent(), &msg);
        emit msgToEndpoint(msg);
    }
}

void RoomTranslator::processDescriptorMessage(QQueue<uint8_t> endpointAddr, const RoomMessage &descMsg)
{
    QList<QByteArray> fields = descMsg.args().split(char(ROOM_BYTE_SEP));

    QString name = QString("Malformed endpoint");
    QVector<RoomItem*> resources;

    //Check length is valid and correct magic string (protocol name)
    if(fields.size() >= ROOM_DESC_MIN_VALID_FIELDS &&
            fields.at(ROOM_DESC_INDEX_MAGICSTRING) == ROOM_MAGIC_STRING)
    {
        name = fields.at(ROOM_DESC_INDEX_EPNAME);

        uint8_t index = 0;
        for(QList<QByteArray>::const_iterator i = fields.constBegin() + ROOM_DESC_MIN_VALID_FIELDS ;
            i != fields.constEnd() ; i++)
        {
            uint8_t resFlag         = i->at(ROOM_DESC_RES_INDEX_FLAG);
            uint8_t resMaxLength    = i->at(ROOM_DESC_RES_INDEX_LENGTH);
            QString resLabel        = i->right(i->length() - ROOM_DESC_RES_INDEX_LABEL);
            resources.append(RoomItem::createResourceItem(index++, resFlag, resMaxLength, resLabel));
        }
    }

    qInfo() << name << "has reset !";

    //qDebug() << "Received descriptor for endpoint" << name << "at address" << printAddress(endpointAddr);

    const RoomItem* updatedEndpoint = m_model->updateEndpoint(endpointAddr, name, resources);

    if(updatedEndpoint->isNode())
    {
        QMetaObject::invokeMethod(this,
                                  "requestStalledEndpointsList",
                                  Qt::QueuedConnection,
                                  Q_ARG(QQueue<uint8_t>,endpointAddr));

//        //Now request descriptor for all resource with ENDPOINT type
//        for(uint8_t i = 0 ; i < resources.count() ; i++)
//        {
//            if(resources.at(i)->isDevice())
//            {
//                QQueue<uint8_t> descriptorAddr = endpointAddr;
//                descriptorAddr.enqueue(i);
//                QMetaObject::invokeMethod(this,
//                                          "requestDescriptor",
//                                          Qt::QueuedConnection,
//                                          Q_ARG(QQueue<uint8_t>,descriptorAddr));
//            }
//        }
    }
}


void RoomTranslator::msgFromEndpoint(RoomMessage msg)
{
    if(!msg.isComplete()) return;
    QQueue<uint8_t> address = extractAddressFromMessage(&msg);
    const RoomItem* endpointItem = m_model->getItemAtAddress(address);

    if(!endpointItem) return;

    // LOGGING
    if(msg.verb() == ROOM_VERB_LOGGING)
    {
        const QString logString = QStringLiteral("%1 : LOG%2 : %3");
        const QString logMsg = stringArgsToValue(msg.args()).toString();
        const QString logSource = endpointItem ? endpointItem->label() : QString("LOG message source is not an endpoint : ");
        qInfo() << logString.arg(logSource, QString::number(msg.noun()), logMsg);
    }

    // DESCRIPTOR
    else if(msg.noun() == ROOM_DESCRIPTOR_ID)
    {
        if(isAcceptedVerbForDescriptor(msg.verb()))
        {
            if(!endpointItem) qWarning() << "Descriptor for non-existing item";
            else processDescriptorMessage(address, msg);
        }
    }

    else if(isAcceptedVerbForValue(msg.verb()))
    {
        // NODE HIDDEN RESOURCES
        if(endpointItem->isNode())
        {
            if(msg.noun() == ROOM_NODE_CMD_STALLED)
            {
                QVector<uint8_t> stalledIds = extractIdsFromByteArray(msg.args());
                m_model->updateStalledIds(address, stalledIds);
            }
        }

        // ACTUAL RESOURCES
        else
        {
            const uint8_t resourceIndex = msg.noun() - 1;
            const RoomItem* resourceItem = endpointItem->child(resourceIndex);
            if(!resourceItem)
            {
                qWarning() << "Value for non-existing resource";
            }
            else
            {
                QVariant value = argsToValue(resourceItem->type(), msg.args());
                bool isValueInitial = msg.verb() == ROOM_VERB_DESC_REQ || msg.verb() == ROOM_VERB_RESET_DONE;
                m_model->updateResourceValue(address, resourceIndex, value, isValueInitial);
            }
        }
    }
}

QVariant RoomTranslator::argsToValue(RoomItem::Type type, QByteArray args)
{
    switch(type)
    {
    case RoomItem::Type_Bool:
        return boolArgsToValue(args);

    case RoomItem::Type_String:
        return stringArgsToValue(args);

    case RoomItem::Type_Integer:
        return integerArgsToValue(args);

    case RoomItem::Type_Color:
        return colorArgsToValue(args);

    default:
        return QVariant();
    }
}

QVariant RoomTranslator::boolArgsToValue(QByteArray args)
{
    return (args.length() == 1) ? QVariant::fromValue(args.at(0) != 0x00) : QVariant();
}

QVariant RoomTranslator::stringArgsToValue(QByteArray args)
{
    return QVariant::fromValue(QString::fromLocal8Bit(args));
}

QVariant RoomTranslator::integerArgsToValue(QByteArray args)
{
    if(args.length() != sizeof (int32_t) ) return QVariant();

    uint32_t value = uint8_t(args.at(0));
    for(uint8_t i = 1 ; i < sizeof (int32_t) ; i++)
    {
        value = (value << 8) | uint8_t(args.at(i));
    }

    return QVariant::fromValue(int32_t(value));
}

QVariant RoomTranslator::colorArgsToValue(QByteArray args)
{
    QColor color;
    if(args.length() == 3)
    {
        color.setRed(uint8_t(args.at(0)));
        color.setGreen(uint8_t(args.at(1)));
        color.setBlue(uint8_t(args.at(2)));
        color.setAlpha(255);
    }
    return color;
}

bool RoomTranslator::sendValue(const RoomItem *resource, QVariant value)
{
    qInfo() << "Request value change" << resource->label()
            << "of" << resource->parent()->label()
            << "to" << value;
    QByteArray args;

    switch(resource->type())
    {
    case RoomItem::Type_None:
        break;

    case RoomItem::Type_Bool:
        args = boolValueToArgs(value);
        break;

    case RoomItem::Type_Color:
        args = colorValueToArgs(value);
        break;

    case RoomItem::Type_Integer:
        args = integerValueToArgs(value);
        break;

    case RoomItem::Type_String:
        args = stringValueToArgs(value);
        break;

    default:
        return false;
    }

    RoomMessage msg(ROOM_VERB_WRITE_EXEC, uint8_t(resource->row()+1), args);
    pushEndpointAddressToMsg(resource->parent(), &msg);
    emit msgToEndpoint(msg);
    return true;
}

QByteArray RoomTranslator::boolValueToArgs(const QVariant &value)
{
    QByteArray args;
    args.append( value.toBool() ? 0x01 : 0x00);
    return args;
}

QByteArray RoomTranslator::stringValueToArgs(const QVariant &value)
{
    return value.toString().toLatin1();
}

QByteArray RoomTranslator::colorValueToArgs(const QVariant & value)
{
    QByteArray a;
    QColor c = value.value<QColor>();
    a.append(c.red());
    a.append(c.green());
    a.append(c.blue());
    return a;
}

QByteArray RoomTranslator::integerValueToArgs(const QVariant &value)
{
    QByteArray a;
    uint32_t intVal = uint32_t(value.toInt());
    for(int8_t i = ((sizeof(intVal)-1) << 3) ; i >= 0 ; i-=8)
    {
        a.append((intVal >> i) & 0xFF);
    }

    return a;
}

void RoomTranslator::pushAddressToMsg(const QQueue<uint8_t> & address, RoomMessage* msg)
{
    QQueue<uint8_t> q = address;
    while(!q.isEmpty())
    {
        msg->pushAddress(q.dequeue());
    }
}


void RoomTranslator::pushEndpointAddressToMsg(const RoomItem *endpoint, RoomMessage *msg)
{
    if(endpoint)
    {
        while(endpoint->parent())
        {
            msg->pushAddress(endpoint->row());
            endpoint = endpoint->parent();
        }
    }
}

QQueue<uint8_t> RoomTranslator::extractAddressFromMessage(RoomMessage *msg)
{
    QQueue<uint8_t> address;
    while(!msg->header().isEmpty())
    {
        address.enqueue(msg->popAddress());
    }
    return address;
}

QString RoomTranslator::printAddress(const QQueue<uint8_t> &address)
{
    QQueue<uint8_t> a = address;
    QString s;
    while(!a.isEmpty())
    {
        s.append('/');
        s.append(QString::number(a.dequeue()));
    }
    return s;
}

QVector<uint8_t> RoomTranslator::extractIdsFromByteArray(const QByteArray a)
{
    QVector<uint8_t> stalledIds(a.length());
    for(uint8_t i = 0 ; i < a.length() ; i++)
    {
        stalledIds[i] = a.at(i);
    }
    return stalledIds;
}

bool RoomTranslator::isAcceptedVerbForDescriptor(uint8_t verb)
{
    switch(verb)
    {
    case ROOM_VERB_ACK:
    case ROOM_VERB_RESET_DONE:
    case ROOM_VERB_VALUE_CHANGED:
        return true;
    default:
        return false;
    }
}

bool RoomTranslator::isAcceptedVerbForValue(uint8_t verb)
{
    switch(verb)
    {
    case ROOM_VERB_ACK:
    case ROOM_VERB_VALUE_CHANGED:
    case ROOM_VERB_DESC_REQ:
    case ROOM_VERB_RESET_DONE:
        return true;
    default:
        return false;
    }
}
