#include "roomclient.h"

#include "roomprotocol.h"
#include "roomtranslator.h"
#include "multiplexer.h"
#include "networkendpoint.h"

#include <QTime>

RoomClient::RoomClient(const QStringList &hosts, QObject *parent) :
    QObject(parent),
    m_system(new RoomSystem(this)),
    m_translator(new RoomTranslator(m_system,this)),
    m_multiplexer(new Multiplexer(QString("Local Mux"), createNetworkEndpoints(hosts), this))
{
    connect(m_system, &RoomSystem::itemValueChanged,
            this, &RoomClient::onItemValueChanged);
    connect(m_translator, &RoomTranslator::msgToEndpoint,
            m_multiplexer, &Multiplexer::msgToEndpoint);
    connect(m_multiplexer, &Multiplexer::msgFromEndpoint,
            m_translator, &RoomTranslator::msgFromEndpoint);

    m_translator->requestDescriptor();


}

RoomSystem *RoomClient::getRoomSystem() const
{
    return m_system;
}


QVector<AbstractEndpoint *> RoomClient::createNetworkEndpoints(const QStringList &hosts)
{
    QVector<AbstractEndpoint *> networkEndpoints;
    foreach(const QString & hostString, hosts)
    {
        QStringList hostParts = hostString.split(':',QString::KeepEmptyParts);
        quint16 hostPort = (hostParts.size() > 1) ? quint16(hostParts.at(1).toInt()) : ROOM_TCP_DEFAULT_PORT;
        networkEndpoints.append(new NetworkEndpoint(hostParts.at(0), hostPort));
    }

    return networkEndpoints;
}

const RoomItem *RoomClient::alterationTargetFinderHelper(const RoomItem *item, const RoomAlteration & alteration)
{
    if(!item->isDevice()) return nullptr;

    // If we have the correct endpoint
    if(item->label() == alteration.endpointName())
    {
        // Look for the correct resource
        for(int i = 0 ; i < item->childrenCount() ; i++)
        {
            const RoomItem* child = item->child(i);
            if(!child->isDevice() && child->label() == alteration.resourceLabel())
            {
                return child;
            }
        }
    }

    // We have not found a matching endpoint / resource
    // Keep looking
    for(int i = 0 ; i < item->childrenCount() ; i++)
    {
        const RoomItem* foundItem = alterationTargetFinderHelper(item->child(i), alteration);
        if(foundItem) return foundItem;
    }

    return nullptr;
}

void RoomClient::onItemValueChanged(const RoomItem *item)
{
    RoomAlteration alteration(item);
    qInfo() << alteration.logString();
    emit roomAltered(alteration);
}

void RoomClient::discoverLocalEndpoints(int interval)
{
    m_multiplexer->doEndpointsDiscovery();
    m_multiplexer->setAutoDiscovery(interval * 1000);
}

bool RoomClient::alterRoom(RoomAlteration alteration)
{
    const RoomItem* resourceItem = alterationTargetFinderHelper(m_system->rootItem(), alteration);
    if(resourceItem == nullptr) return false;
    emit m_system->sendValue(resourceItem, alteration.value());
    return true;
}

