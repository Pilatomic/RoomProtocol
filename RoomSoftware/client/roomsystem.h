#ifndef ROOMSYSTEM_H
#define ROOMSYSTEM_H

#include "roommessage.h"
#include "roomalteration.h"

#include <QQueue>
#include <QVector>

class RoomItem;

class RoomSystem : public QObject
{
    Q_OBJECT

public:
    explicit RoomSystem(QObject *parent = nullptr);
    ~RoomSystem() override;

    const RoomItem *rootItem() const;

    const RoomItem *getItemAtAddress(QQueue<uint8_t> address);
    const RoomItem *updateEndpoint(QQueue<uint8_t> address, QString name, QVector<RoomItem*> resources);
    void updateResourceValue(QQueue<uint8_t> address, uint8_t resourceId, QVariant value, bool isInitial = false);
    void updateStalledIds(QQueue<uint8_t> address, const QVector<uint8_t> stalledIds);

signals:
    void sendValue(const RoomItem* resource, QVariant value);
    void requestEndpointReset(const RoomItem* endpoint);

    void itemUpdatePrepare(const RoomItem* endpoint);
    void itemUpdateHalfWay(int nextChildrenCount);
    void itemUpdateCompleted();
    void itemLabelChanged(const RoomItem* item);
    void itemValueChanged(const RoomItem* item);
    void itemIsStalledChanged(const RoomItem* item);

protected:
    RoomItem* itemFromAddress(QQueue<uint8_t> address);
    QModelIndex createIndexForItem(RoomItem* item) const;

    void updateStalledChildren(RoomItem* item, bool isStalled);

private:
    RoomItem *m_rootItem;

};

#endif // ROOMSYSTEM_H
