#ifndef ROOMITEM_H
#define ROOMITEM_H

#include <QVector>
#include <QVariant>

#include "roomprotocol.h"

class RoomItem
{
    Q_GADGET
public:
    enum Type
    {
        Type_None       = ROOM_DESC_TYPE_NONE,
        Type_Bool       = ROOM_DESC_TYPE_BOOL,
        Type_String     = ROOM_DESC_TYPE_STRING,
        Type_Integer    = ROOM_DESC_TYPE_INTEGER,
        Type_Color      = ROOM_DESC_TYPE_COLOR,
        Type_Endpoint   = ROOM_DESC_TYPE_ENDPOINT,
    };
    Q_ENUM(Type)

    enum Capabilities
    {
        Cap_None        = 0x00,
        Cap_Notif       = 0x80,
        Cap_Exec        = 0x40,
        Cap_NotifExec   = Cap_Notif | Cap_Exec
    };
    Q_ENUM(Capabilities)

    static RoomItem *createRootItem();
    static RoomItem *createResourceItem(uint8_t row, uint8_t flags, uint8_t length, const QString & label);

    ~RoomItem();

    uint8_t row() const;

    Type type() const;
    Capabilities caps() const;

    QString label() const;
    void setLabel(const QString newLabel);

    uint8_t maxLength() const;

    const QVariant & value() const;
    void setValue(QVariant value);

    void setIsInitialvalue(bool b);
    bool isInitialValue() const;

    RoomItem *parent() const;
    void setParent(RoomItem* parent);

    RoomItem *child(int i) const;
    int childrenCount() const;

    bool isStalled() const;
    void setStalled(bool isStalled);

    bool isDevice() const;
    bool isNode() const;

    void clearChildrens();
    bool updateChildrens(QVector<RoomItem*> childrens);

protected :
    explicit RoomItem();

private:
    RoomItem* m_parent = nullptr;
    uint8_t m_row = 0;
    uint8_t m_flags = 0;
    QString m_label;
    QVariant m_value;
    uint8_t m_maxLength = 0;
    QVector<RoomItem*> m_childrens;
    bool m_stalled = false;
    bool m_isNode = false;
    bool m_isInitialValue = false;
};

#endif // ROOMITEM_H
