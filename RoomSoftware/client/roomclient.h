#ifndef ROOMCLIENT_H
#define ROOMCLIENT_H

#include <QObject>
#include <QStringList>

#include "roomalteration.h"
#include "roomsystem.h"
#include "roomitem.h"
#include <QAbstractItemModel>

class AbstractEndpoint;
class RoomTranslator;
class Multiplexer;
class NetworkEndpoint;

class RoomClient : public QObject
{
    Q_OBJECT
public:
    explicit RoomClient(const QStringList &hosts, QObject *parent = nullptr);

    RoomSystem *getRoomSystem() const;

    void discoverLocalEndpoints(int interval);

signals:
    void roomAltered(RoomAlteration alteration);

public slots:
    bool alterRoom(RoomAlteration alteration);

private slots:
    void onItemValueChanged(const RoomItem* item);

protected:
    static QVector<AbstractEndpoint *> createNetworkEndpoints(const QStringList &hosts);
    const RoomItem *alterationTargetFinderHelper(const RoomItem* item, const RoomAlteration &alteration);


private:
    RoomSystem* m_system;
    RoomTranslator* m_translator;
    Multiplexer* m_multiplexer;
};

#endif // ROOMCLIENT_H
