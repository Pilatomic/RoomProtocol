#ifndef ROOMLINK_H
#define ROOMLINK_H

#include "roommessage.h"

#include <QIODevice>
#include <QObject>
#include <QPointer>
#include <QTimer>

class RoomLink : public QObject
{
    Q_OBJECT
public:
    explicit RoomLink(QObject *parent = nullptr);
    void setDevice(QIODevice* device);

    void transmitMsg(const RoomMessage &msg);

    void setAutoPing(int delayPing, int delayPong);

    void requestDescriptor();

signals:
    void msgReceived(RoomMessage msg);
    void pongReceived(bool received);

private slots:
    void sendPing();
    void onPongReceived();
    void timedOut();

private:
    void readDeviceData();
    QByteArray buildByte(char c);

    QByteArray m_rxFrameBytes;
    bool m_isNextByteEscaped;

    QPointer<QIODevice> m_device;

    QTimer* m_pingTimer = nullptr;
    QTimer* m_pongTimer = nullptr;
    bool m_pongReceived = false;
};

#endif // ROOMLINK_H
