#include "roomlink.h"
#include "roomprotocol.h"

#include <QDebug>


RoomLink::RoomLink(QObject *parent) :
    QObject(parent),
    m_isNextByteEscaped(false)
{
}

void RoomLink::setDevice(QIODevice *device)
{
    if(!m_device.isNull()) m_device->disconnect(this);
    m_device = device;
    if(!m_device.isNull())
    {
        connect(m_device.data(), &QIODevice::readyRead, this, &RoomLink::readDeviceData);
        if(m_pingTimer && m_pongTimer) sendPing();
    }
}

void RoomLink::readDeviceData()
{
    QByteArray bytes = m_device->readAll();

    for(QByteArray::const_iterator it = bytes.cbegin() ;
        it != bytes.cend() ;
        it++)
    {
        uint8_t c = *it;

        switch(c)
        {
        case ROOM_BYTE_PING:
            m_device->write(buildByte(char(ROOM_BYTE_PONG)));
            break;
        case ROOM_BYTE_PONG:
            onPongReceived();
            break;
        case ROOM_BYTE_END:
            emit msgReceived(RoomMessage(m_rxFrameBytes));
            m_rxFrameBytes.clear();
            m_isNextByteEscaped = false;
            break;
        case ROOM_BYTE_START:
            m_rxFrameBytes.clear();
            m_isNextByteEscaped = false;
            break;
        case ROOM_BYTE_ESC:
            m_isNextByteEscaped = true;
            break;
        default:
            m_rxFrameBytes.append(m_isNextByteEscaped ? (c ^ 0xFF) : c);
            m_isNextByteEscaped = false;
            break;
        }

        //Prevent ram overflow attack
        m_rxFrameBytes.truncate(ROOM_MAX_LENGTH_NODE);
    }
}

QByteArray RoomLink::buildByte(char c)
{
    QByteArray a;
    a.append(c);
    return a;
}


void RoomLink::transmitMsg(const RoomMessage &msg)
{
    if(m_device.isNull() || !msg.isValid()) return;

    QByteArray messageBytes = msg.bytes();

    QByteArray wireBytes;
    wireBytes.append(ROOM_BYTE_START);
        for(QByteArray::const_iterator c = messageBytes.constBegin();
        c != messageBytes.constEnd();
        c++)
    {
        if(((*c)&ROOM_RESERVED_BITS) == ROOM_RESERVED_VALUE)
        {
            wireBytes.append(ROOM_BYTE_ESC);
            wireBytes.append((*c) ^ 0xFF);
        }
        else
        {
            wireBytes.append(*c);
        }
    }
    wireBytes.append(ROOM_BYTE_END);

    if(m_device->write(wireBytes) < 0)
    {
        qDebug() << "RoomLink : Error writing message to device";
    }
}

void RoomLink::setAutoPing(int delayPing, int delayPong)
{
    if(!m_pingTimer)
    {
        m_pingTimer = new QTimer(this);
        m_pingTimer->setSingleShot(true);
        connect(m_pingTimer, &QTimer::timeout, this, &RoomLink::sendPing);
    }
    if(!m_pongTimer)
    {
        m_pongTimer = new QTimer(this);
        m_pongTimer->setSingleShot(true);
        connect(m_pongTimer, &QTimer::timeout, this ,&RoomLink::timedOut);
    }

    //Stop on delay pong negative or null value
    if(delayPong <= 0)
    {
        m_pingTimer->stop();
        m_pongTimer->stop();
        delete m_pingTimer;
        m_pingTimer = nullptr;
        delete m_pongTimer;
        m_pongTimer = nullptr;
        return;
    }

    m_pingTimer->setInterval(delayPing);
    m_pongTimer->setInterval(delayPong);

    sendPing();
}

void RoomLink::sendPing()
{
    if(m_device.isNull()) return;
    m_pongReceived = false;
    m_device->write(buildByte(ROOM_BYTE_PING));
    m_pongTimer->start();
}

void RoomLink::onPongReceived()
{
    m_pongReceived = true;
    m_pongTimer->stop();
    m_pingTimer->start();
    emit pongReceived(true);
}

void RoomLink::timedOut()
{
    readDeviceData();
    if(!m_pongReceived)
    {
        m_pingTimer->start();
        emit pongReceived(false);
    }
}


void RoomLink::requestDescriptor()
{
    transmitMsg(RoomMessage(ROOM_VERB_READ,ROOM_DESCRIPTOR_ID));
}
