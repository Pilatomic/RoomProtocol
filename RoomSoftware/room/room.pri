
if (!contains( included_modules, $$PWD )) {
    included_modules += $$PWD
    message( "Including $$PWD" )

    QT += core

    CONFIG += c++11

    INCLUDEPATH += $$PWD

    SOURCES += \
        $$PWD/roomlink.cpp \
        $$PWD/roommessage.cpp

    HEADERS += \
        $$PWD/roomprotocol.h \
        $$PWD/roomlink.h \
        $$PWD/roommessage.h


} else {
    message( "Skipping $$PWD: already included" )
}
