#include "qmlobject.h"

#include <QtDebug>

QmlObject::QmlObject(QObject *parent) : QObject(parent)
{

}

void QmlObject::setQmlParent(QmlObject* parent)
{
    QObject::setParent(parent);
    emit parentChanged();
}

QQmlListProperty<QObject> QmlObject::data()
{
    return QQmlListProperty<QObject>(this, this,
                 &QmlObject::data_append,
                 &QmlObject::data_count,
                 &QmlObject::data_at,
                                     &QmlObject::data_clear);
}


void QmlObject::data_append(QQmlListProperty<QObject>* list, QObject* p) {

    QmlObject* qmlObj = qobject_cast<QmlObject*>(p);
    if(qmlObj) qmlObj->setQmlParent(reinterpret_cast< QmlObject* >(list->data));
    else p->setParent(reinterpret_cast< QmlObject* >(list->data));

    //If we have a resource object !
    /*QmlResource *resource = qobject_cast<QmlResource*>(p);
    if(resource)
    {
        qDebug() << "We have a resource object" << resource->property("endpointString");
    }*/
}

int QmlObject::data_count(QQmlListProperty<QObject>* list) {
    return reinterpret_cast< QmlObject* >(list->data)->children().count();
}

QObject* QmlObject::data_at(QQmlListProperty<QObject>* list, int i) {
    return reinterpret_cast< QmlObject* >(list->data)->children().at(i);
}

void QmlObject::data_clear(QQmlListProperty<QObject>* list) {
    const QObjectList & children = reinterpret_cast< QmlObject* >(list->data)->children();
    foreach(QObject* child, children)
    {
        delete  child;
    }
}
