import QtQml 2.8
import RoomScript 1.0


RoomScript {
    property color currColor: selectColor(t.counter)

    function selectColor(i)
    {
        var u = i%8;
        switch(u){
        case 0:
            return "#000000";
        case 1:
            return "#FF0000";
        case 2:
            return "#00FF00";
        case 3:
            return "#FFFF00";
        case 4:
            return "#0000FF";
        case 5:
            return "#FF00FF";
        case 6:
            return "#00FFFF";
        case 7:
            return "#FFFFFF";
        }
    }

    SoundEffect {
        id: sound1
        source:"file:///home/pila/Bureau/job-done.wav"
    }

/*    RoomResource {
        id: testRoomLight
        endpoint: "TestRoom"
        resource: "Lighting"
        value: currColor
    }*/

    RoomResource {
        id: button1Color
        endpoint: "CubeTest"
        resource: "UpColor"
        value: currColor
    }

    RoomResource {
        id: button1Pressed
        endpoint: "CubeTest"
        resource: "UpPressed"
        onValueChanged: if(value === true) sound1.play()
    }

    RoomResource {
        id: button2Color
        endpoint: "CubeTest"
        resource: "DownColor"
        value: currColor
    }

    RoomResource {
        id: button2Pressed
        endpoint: "CubeTest"
        resource: "DownPressed"
        onValueChanged: if(value === true) sound1.play()
    }

    RoomResource {
        id: button3Color
        endpoint: "CubeTest"
        resource: "LeftColor"
        value: currColor
    }

    RoomResource {
        id: button3Pressed
        endpoint: "CubeTest"
        resource: "LeftPressed"
        onValueChanged: if(value === true) sound1.play()
    }

    RoomResource {
        id: button4Color
        endpoint: "CubeTest"
        resource: "RightColor"
        value: currColor
    }

    RoomResource {
        id: button4Pressed
        endpoint: "CubeTest"
        resource: "RightPressed"
        onValueChanged: if(value === true) sound1.play()
    }

    Timer {
        id: t
        running: true
        interval: 500
        repeat: true
        property int counter: 0;
        onTriggered: {
            counter++;
        }
    }


}




/*RoomScript {
    RoomResource {
        id: resource1
        endpoint: "Cube"
        resource: "Release"
    }

    RoomResource {
        id: resource2
        endpoint: "Cube1"
        resource: "Lock Status"
        onValueChanged: console.log(endpoint + " " + resource + " : " + value)
    }

    RoomResource {
        id: cubeLocked
        endpoint: "Cube1"
        resource: "Locked"
        onValueChanged:
        {
            if(value === true) sound1.play()
            console.log(endpoint + " " + resource + " : " + value)
        }
    }

    RoomResource {
        id: cubeColor
        endpoint: "Cube1"
        resource: "LeftColor"
        value: cubeLocked.value ? "#00FF00" : "#FF0000"
    }

    RoomResource {
        id: cubeColor2
        endpoint: "Cube1"
        resource: "RightColor"
        value: cubeLocked.value ? "#00FF00" : "#FF0000"
    }

    RoomResource {
        id: cubeButton
        endpoint: "Cube1"
        resource: "LeftPressed"
        onValueChanged: if(value === true) resource1.value = 0
    }

    RoomResource {
        id: demoLed
        endpoint: "ROOM DEMO DEVICE"
        resource: "LED"
    }

    Binding {
        target: demoLed; property: 'value'
        value: cubeLocked.value
        delayed: true
    }

    SoundEffect {
        id: sound1
        source:"file:///home/pila/Bureau/job-done.wav"
    }
}*/


