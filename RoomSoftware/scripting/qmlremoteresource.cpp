#include "qmlremoteresource.h"
#include "roomscriptremoteinterface.h"

#include <QQmlEngine>
#include <QQmlContext>
#include <QtDebug>

QmlRemoteResource::QmlRemoteResource(QObject *parent) : QmlObject(parent)
{

}

QmlRemoteResource::~QmlRemoteResource()
{
    if(m_interface != nullptr) m_interface->unregisterRemoteResource(this);
}

const QString &QmlRemoteResource::getEndpointString() const
{
    return m_endpointString;
}

void QmlRemoteResource::setEndpointString(const QString & endpointString)
{
    if(endpointString == m_endpointString) return;
    m_endpointString = endpointString;
    if(m_interface != nullptr) m_interface->registerRemoteResource(this);
    emit endpointStringChanged(m_endpointString);
}

const QString &QmlRemoteResource::getResourceString() const
{
    return m_resourceString;
}

void QmlRemoteResource::setResourceString(const QString & resourceString)
{
    if(resourceString == m_resourceString) return;
    m_resourceString = resourceString;
    if(m_interface != nullptr) m_interface->registerRemoteResource(this);
    emit resourceStringChanged(m_resourceString);
}

const QVariant &QmlRemoteResource::getValue() const
{
    return m_actualValue;
}

void QmlRemoteResource::setValue(const QVariant &value)
{
    //Do not set if already done
    // (avoid spamming of requests when QML expression is reevaluated to the same value)
    if(value == m_requestedValue && value == m_actualValue) return;

    m_requestedValue = value;
    //qDebug() << "QmlRemoteResource : Set value of" << m_endpointString << m_resourceString << "to" << m_requestedValue;
    if(m_interface != nullptr) m_interface->resourceValueChangeRequest(this, m_requestedValue);
}

void QmlRemoteResource::receivedNewValue(const QVariant &value)
{
    m_actualValue = value;
    // Only delete requested value if newly received value is different
    // so we keep the requested value to apply if again in case of endpoint reset
    if(m_actualValue != m_requestedValue) m_requestedValue.clear();
    emit valueChanged(m_actualValue);
    //qDebug() << "RemoteResource" << m_endpointString << m_resourceString << m_value;
}

const QVariant &QmlRemoteResource::getRequestedValue() const
{
    return m_requestedValue;
}

void QmlRemoteResource::exec()
{
    if(m_interface != nullptr) m_interface->resourceValueChangeRequest(this, QVariant());
}


void QmlRemoteResource::classBegin()
{
}

void QmlRemoteResource::componentComplete()
{
    QQmlContext* localContext = QQmlEngine::contextForObject(this);
    Q_ASSERT(localContext != nullptr);

    const QVariant & v = localContext->contextProperty("remoteinterface");
    QObject* intfObj = qvariant_cast<QObject*>(v);
    Q_ASSERT(intfObj != nullptr);

    m_interface = qobject_cast<RoomScriptRemoteInterface*>(intfObj);
    Q_ASSERT(m_interface != nullptr);

    m_interface->registerRemoteResource(this);
}
