#ifndef QMLREMOTERESOURCE_H
#define QMLREMOTERESOURCE_H

#include "qmlobject.h"

#include <QObject>
#include <QVariant>
#include <QQmlParserStatus>

class RoomScriptRemoteInterface;

class QmlRemoteResource : public QmlObject, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(QString endpoint
               READ getEndpointString
               WRITE setEndpointString
               NOTIFY endpointStringChanged)
    Q_PROPERTY(QString resource
               READ getResourceString
               WRITE setResourceString
               NOTIFY resourceStringChanged)
    Q_PROPERTY(QVariant value
               READ getValue
               WRITE setValue
               NOTIFY valueChanged)

public:
    explicit QmlRemoteResource(QObject *parent = nullptr);
    ~QmlRemoteResource();

    const QString & getEndpointString() const;
    void setEndpointString(const QString & endpointString);

    const QString & getResourceString() const;
    void setResourceString(const QString & resourceString);

    const QVariant & getValue() const;
    void setValue(const QVariant & value);

// only use these from RemoteInterface
    void receivedNewValue(const QVariant & value);
    const QVariant &getRequestedValue() const;

    Q_INVOKABLE void exec();

    // QQmlParserStatus interface
public:
    void classBegin() override;
    void componentComplete() override;

signals:
    void endpointStringChanged(const QString & endpointString);
    void resourceStringChanged(const QString & resourceString);
    void valueChanged(const QVariant & value);

private:
    QString m_endpointString;
    QString m_resourceString;
    QVariant m_actualValue;
    QVariant m_requestedValue;
    RoomScriptRemoteInterface* m_interface = nullptr;
};

#endif // QMLREMOTERESOURCE_H
