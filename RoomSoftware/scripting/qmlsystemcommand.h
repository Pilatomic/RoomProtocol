#ifndef QMLSYSTEMCOMMAND_H
#define QMLSYSTEMCOMMAND_H

#include <QObject>
#include "qmlobject.h"

class QProcess;

class QmlSystemCommand : public QmlObject
{
    Q_OBJECT
    Q_PROPERTY(QString command
               READ getCommand
               WRITE setCommand
               NOTIFY commandChanged)

    Q_PROPERTY(bool running
               READ isRunning
               NOTIFY runningChanged)

    Q_PROPERTY(bool looped
               READ isLooped
               WRITE setLooped
               NOTIFY loopedChanged)


public:
    explicit QmlSystemCommand(QObject *parent = nullptr);

    bool isRunning() const;

    const QString &getCommand() const;

    bool isLooped() const;

public slots:

    void start();
    void stop();
    void setCommand(const QString & command);
    void setLooped(bool b);

signals:
    void commandChanged(const QString & command);
    void runningChanged(bool running);
    void loopedChanged(bool looped);

private:
    void onProcessStateChanged();
    void onProcessFinished();

    QProcess* m_process = nullptr;
    QString m_command;
    bool m_looped = false;
    bool m_running = false;
};

#endif // QMLSYSTEMCOMMAND_H
