#ifndef QMLSCRIPT_H
#define QMLSCRIPT_H

#include "qmlobject.h"

#include <QObject>

class QmlScript : public QmlObject
{
    Q_OBJECT

public:
    explicit QmlScript(QObject *parent = nullptr);
};

#endif // QMLSCRIPT_H
