#include "roomscriptengine.h"
#include "roomscriptremoteinterface.h"
#include "qmlremoteresource.h"
#include "qmlobject.h"
#include "qmlscript.h"
#include "qmlsystemcommand.h"

#include <QQmlComponent>
#include <QQmlEngine>
#include <QQmlContext>
#include <QtDebug>
#include <QSoundEffect>
#include <QFileInfo>


bool RoomScriptEngine::typesAlreadyRegistered = false;

RoomScriptEngine::RoomScriptEngine(RoomSystem *roomSystem, const QString &scriptPath, QObject *parent) : QObject(parent)
{
    if(!typesAlreadyRegistered)
    {
        qmlRegisterType<QmlObject>("RoomScript", 1, 0, "ScriptObject");
        qmlRegisterType<QmlScript>("RoomScript", 1, 0, "RoomScript");
        qmlRegisterType<QmlRemoteResource>("RoomScript", 1, 0, "RoomResource");
        qmlRegisterType<QSoundEffect>("RoomScript", 1, 0, "SoundEffect");
        qmlRegisterType<QmlSystemCommand>("RoomScript", 1, 0 ,"SystemCommand");
        typesAlreadyRegistered = true;
    }

    m_remoteInterface = new RoomScriptRemoteInterface(roomSystem, this);

    QFileInfo qmlPathInfo(scriptPath);
    QString scriptFolderPath = qmlPathInfo.absolutePath();

    m_engine = new QQmlEngine(this);
    m_engine->setImportPathList(QStringList());
    m_engine->rootContext()->setContextProperty("remoteinterface", m_remoteInterface);
    m_engine->rootContext()->setContextProperty("scriptpath",scriptFolderPath.append("/"));

    m_component = new QQmlComponent(m_engine, scriptPath,
                                    QQmlComponent::PreferSynchronous,
                                    this);
    m_component->create();


    auto errorList = m_component->errors();
    if(errorList.isEmpty())     qInfo() << "Loaded script" << scriptPath;
    else qWarning() << "Failed to load" << scriptPath;
    foreach(const QQmlError& err, errorList)
    {
        qWarning() << err.toString();
    }
}
