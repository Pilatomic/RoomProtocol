#include "roomscriptremoteinterface.h"

#include "qmlremoteresource.h"

#include "roomsystem.h"
#include "roomitem.h"

#include <QtDebug>

RoomScriptRemoteInterface::RoomScriptRemoteInterface(RoomSystem *roomSystem, QObject *parent) : QObject(parent)
{
    m_roomSystem = roomSystem;
    connect(m_roomSystem, &RoomSystem::itemUpdatePrepare,
            this, &RoomScriptRemoteInterface::itemUpdatePrepare);
    connect(m_roomSystem, &RoomSystem::itemUpdateHalfWay,
            this, &RoomScriptRemoteInterface::itemUpdateHalfWay);
    connect(m_roomSystem, &RoomSystem::itemUpdateCompleted,
            this, &RoomScriptRemoteInterface::itemUpdateCompleted);
    connect(m_roomSystem, &RoomSystem::itemLabelChanged,
            this, &RoomScriptRemoteInterface::itemLabelChanged);
    connect(m_roomSystem, &RoomSystem::itemValueChanged,
            this, &RoomScriptRemoteInterface::itemValueChanged);
    connect(m_roomSystem, &RoomSystem::itemIsStalledChanged,
            this, &RoomScriptRemoteInterface::itemIsStalledChanged);
}

void RoomScriptRemoteInterface::registerRemoteResource(QmlRemoteResource *remoteResource)
{
    Q_ASSERT(!m_qmlToRoomMap.contains(remoteResource));
    const RoomItem* roomResource = nullptr;

    //Find endpoint with correct label, then check if one of his children is the resource we want
    const RoomItem* endpoint = getKnownEndpointFromLabel(remoteResource->getEndpointString());
    if(endpoint)
    {
        for(int i = 0 ; i < endpoint->childrenCount() ; i++)
        {
            const RoomItem* currRes = endpoint->child(i);
            if(currRes->label() == remoteResource->getResourceString())
            {
                roomResource = currRes;
                break;
            }
        }
    }

    m_qmlToRoomMap.insert(remoteResource, roomResource);
    if(roomResource)
    {
        m_roomToQmlMap.insert(roomResource, remoteResource);
        transmitRequestedValue(remoteResource);
    }
    //qDebug() << "Registered resource" << remoteResource->getEndpointString() << remoteResource->getResourceString();
}

void RoomScriptRemoteInterface::unregisterRemoteResource(QmlRemoteResource *remoteResource)
{
    qWarning() << "Dynamically removing resources from script is not supported yet !";
    Q_ASSERT(true);
}

void RoomScriptRemoteInterface::resourceValueChangeRequest(QmlRemoteResource *remoteResource, const QVariant &value)
{
    //qDebug()  << "resourceValueChangeRequest"   << remoteResource->property("id") << remoteResource->getValue();

    const RoomItem* matchingRoomResource = m_qmlToRoomMap.value(remoteResource, nullptr);
    if(matchingRoomResource != nullptr)
    {
        m_roomSystem->sendValue(matchingRoomResource, value);
    }
}

void RoomScriptRemoteInterface::itemUpdatePrepare(const RoomItem *endpoint)
{
    removeItemChildren(endpoint);
    m_itemUpdating = endpoint;
}

void RoomScriptRemoteInterface::itemUpdateHalfWay(int nextChildrenCount)
{
    Q_UNUSED(nextChildrenCount);
}

void RoomScriptRemoteInterface::itemUpdateCompleted()
{
    const RoomItem* itemCompleted = m_itemUpdating;
    m_itemUpdating = nullptr;
    m_endpointList.append(m_itemUpdating);
    mapMatchingItemResources(itemCompleted);
}

void RoomScriptRemoteInterface::itemLabelChanged(const RoomItem *item)
{
    //Item changing label means we must redo mapping for this item
    for(int i = 0 ; i < item->childrenCount() ; i++)
    {
        removeItem(item->child(i));
    }
    mapMatchingItemResources(item);
}

void RoomScriptRemoteInterface::itemValueChanged(const RoomItem *item)
{
    QList<QmlRemoteResource*> matchingQmlResList = m_roomToQmlMap.values(item);
    foreach(QmlRemoteResource* qmlRes, matchingQmlResList)
    {
        // Only notify QML if not initial value
        // (if initial value, QML will be notified in mapMatchingResources)
        // This behavior is necessary to ensure values set from QML
        // can be reapplied in case of endpoint reset
         bool isQmlResValueRequested = item->isInitialValue() && qmlRes->getRequestedValue().isValid();
         if(!isQmlResValueRequested ) qmlRes->receivedNewValue(item->value());
        //if(!item->isInitialValue()) qmlRes->receivedNewValue(item->value());
    }
}

void RoomScriptRemoteInterface::itemIsStalledChanged(const RoomItem *item)
{
    Q_UNUSED(item)
}

void RoomScriptRemoteInterface::removeItemChildren(const RoomItem *item)
{
    //if(!item->isDevice()) return;

    removeItem(item);

    // Explore all children (maybe test if endpoint is Node before for optimisation ?)
    for(int i = 0 ; i < item->childrenCount() ; i++)
    {
        removeItemChildren(item->child(i));
    }
}

void RoomScriptRemoteInterface::removeItem(const RoomItem *item)
{
    QList<QmlRemoteResource*> matchingQmlRes = m_roomToQmlMap.values(item);
    m_roomToQmlMap.remove(item);

    //Remove item from endpoint list
    if(item->isDevice())
    {
        int knownEndpointIndex = m_endpointList.indexOf(item);
        if(knownEndpointIndex >= 0) m_endpointList.remove(knownEndpointIndex);
    }

    foreach(QmlRemoteResource* remoteRes,matchingQmlRes)
    {
        m_qmlToRoomMap[remoteRes] = nullptr;
    }
}

void RoomScriptRemoteInterface::mapMatchingItemResources(const RoomItem *parentItem)
{
    const QString & endpointString = parentItem->label();

    //Map all qml resources for that endpoint using their resource label
    QMap<QString, QmlRemoteResource*> resourceLabelToQml;
    for(auto i = m_qmlToRoomMap.begin() ; i != m_qmlToRoomMap.end() ; i++)
    {
        QmlRemoteResource* currResPtr = i.key();
        if(currResPtr->getEndpointString() == endpointString)
        {
            resourceLabelToQml.insert(currResPtr->getResourceString(), currResPtr);
        }
    }

    //Scan every children and map matching resources
    for(int i = 0 ; i < parentItem->childrenCount() ; i++)
    {
        const RoomItem* childItem = parentItem->child(i);
        if(childItem->isDevice()) continue; // No way this is a resource
        QmlRemoteResource* matchingQmlRes = resourceLabelToQml.value(childItem->label(), nullptr);
        if(matchingQmlRes != nullptr)
        {
            m_qmlToRoomMap[matchingQmlRes] = childItem;
            m_roomToQmlMap.insert(childItem, matchingQmlRes);
            transmitRequestedValue(matchingQmlRes);
        }
    }
}

const RoomItem* RoomScriptRemoteInterface::getKnownEndpointFromLabel(const QString label)
{

    for(auto epIt = m_endpointList.cbegin() ; epIt != m_endpointList.cend() ; epIt++)
    {
        if((*epIt)->label() == label) return *epIt;
    }
    return nullptr;
}

void RoomScriptRemoteInterface::transmitRequestedValue(QmlRemoteResource *qmlRes)
{
    const RoomItem* roomRes = m_qmlToRoomMap.value(qmlRes);

    Q_ASSERT(roomRes != nullptr);

    // If value is set from QML, transmit requested value now,
    // else push known value to QML
    const QVariant & resReqValue = qmlRes->getRequestedValue();
    if(resReqValue.isValid())
    {
        m_roomSystem->sendValue(roomRes, resReqValue);
        //qDebug() << "Sent overriden value" << matchingQmlRes->getValue();
    }
}
