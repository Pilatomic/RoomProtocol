#ifndef ROOMSCRIPTREMOTEINTERFACE_H
#define ROOMSCRIPTREMOTEINTERFACE_H

#include <QObject>
#include <QMap>
#include <QMultiMap>
#include <QVector>


class QmlRemoteResource;
class RoomSystem;
class RoomItem;

class RoomScriptRemoteInterface : public QObject
{
    Q_OBJECT
public:
    explicit RoomScriptRemoteInterface(RoomSystem* roomSystem, QObject *parent = nullptr);

    void registerRemoteResource(QmlRemoteResource* remoteResource);
    void unregisterRemoteResource(QmlRemoteResource* remoteResource);
    void resourceValueChangeRequest(QmlRemoteResource* remoteResource, const QVariant & value);

public slots:
    void itemUpdatePrepare(const RoomItem* endpoint);
    void itemUpdateHalfWay(int nextChildrenCount);
    void itemUpdateCompleted();
    void itemLabelChanged(const RoomItem* item);
    void itemValueChanged(const RoomItem* item);
    void itemIsStalledChanged(const RoomItem* item);

private:
    void removeItemChildren(const RoomItem* item);
    void removeItem(const RoomItem* item);
    void mapMatchingItemResources(const RoomItem* item);
    const RoomItem* getKnownEndpointFromLabel(const QString label);
    void transmitRequestedValue(QmlRemoteResource* qmlRes);

    RoomSystem * m_roomSystem = nullptr;
    QMultiMap<const RoomItem*, QmlRemoteResource*> m_roomToQmlMap;
    QMap<QmlRemoteResource*, const RoomItem*> m_qmlToRoomMap;
    QVector<const RoomItem*> m_endpointList;
    const RoomItem* m_itemUpdating = nullptr;
};

#endif // ROOMSCRIPTREMOTEINTERFACE_H
