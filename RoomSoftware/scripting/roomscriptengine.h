#ifndef ROOMSCRIPTENGINE_H
#define ROOMSCRIPTENGINE_H

#include <QObject>

class QQmlEngine;
class QQmlComponent;
class QQmlContext;
class RoomScriptRemoteInterface;
class QmlContextObject;
class RoomSystem;

class RoomScriptEngine : public QObject
{
    Q_OBJECT
public:
    explicit RoomScriptEngine(RoomSystem* roomSystem, const QString & scriptPath, QObject *parent = nullptr);

signals:

public slots:

private:
    QQmlEngine *m_engine = nullptr;
    QQmlComponent *m_component = nullptr;
    RoomScriptRemoteInterface *m_remoteInterface = nullptr;


    static bool typesAlreadyRegistered;
};

#endif // ROOMSCRIPTENGINE_H
