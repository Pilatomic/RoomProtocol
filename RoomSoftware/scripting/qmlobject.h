#ifndef QMLOBJECT_H
#define QMLOBJECT_H

#include <QObject>
#include <QQmlListProperty>
#include <QVector>

class QmlObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<QObject> data READ data)
    Q_PROPERTY(QObject* parent READ parent WRITE setParent NOTIFY parentChanged)
    Q_CLASSINFO("DefaultProperty", "data")

public:
    explicit QmlObject(QObject *parent = nullptr);
    void setQmlParent(QmlObject *parent);

    QQmlListProperty<QObject> data();

signals:
    void parentChanged();

private:
    static void data_append(QQmlListProperty<QObject>*, QObject*);
    static int data_count(QQmlListProperty<QObject>*);
    static QObject* data_at(QQmlListProperty<QObject>*, int);
    static void data_clear(QQmlListProperty<QObject>*);

    QVector<QObject*> m_data;
};

#endif // QMLOBJECT_H
