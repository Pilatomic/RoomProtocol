#include "qmlsystemcommand.h"
#include <QProcess>
#include <QDebug>

QmlSystemCommand::QmlSystemCommand(QObject *parent) :
    QmlObject(parent)
{
    m_process = new QProcess(this);
    connect(m_process, &QProcess::stateChanged,
            this, &QmlSystemCommand::onProcessStateChanged);
    connect(m_process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            this, &QmlSystemCommand::onProcessFinished);
}

void QmlSystemCommand::start()
{
    m_running = true;
    m_process->start(m_command);
}

void QmlSystemCommand::stop()
{
    m_running = false;
    m_process->kill();
}

bool QmlSystemCommand::isRunning() const
{
    return m_process->state() != QProcess::NotRunning;
}

const QString &QmlSystemCommand::getCommand() const
{
    return m_command;
}

bool QmlSystemCommand::isLooped() const
{
    return m_looped;
}

void QmlSystemCommand::setCommand(const QString &command)
{
    m_command = command;
}

void QmlSystemCommand::setLooped(bool b)
{
    if(b != m_looped)
    {
        m_looped = b;
        emit loopedChanged(m_looped);
    }
}

void QmlSystemCommand::onProcessStateChanged()
{
    emit runningChanged(isRunning());
}

void QmlSystemCommand::onProcessFinished()
{
    if(m_looped && m_running) start();
}
