#ifndef ENDPOINTFACTORY_H
#define ENDPOINTFACTORY_H

#include <QVector>
#include <QString>

class AbstractEndpoint;
class QSettings;

class EndpointFactory
{

public:
    static QVector<AbstractEndpoint *> createEndpointsFromFile(const QString & filePath);

private:
    EndpointFactory();
    static AbstractEndpoint *createEndpointFromSettings(const QSettings *settings);

    typedef AbstractEndpoint*(*EndpointCreatorFunc)(const QSettings* settings);
    typedef QMap<QString, EndpointCreatorFunc> EndpointCreatorMap;

    static const EndpointCreatorMap creatorMap;

    //Endpoint-specific creator funcs
    static AbstractEndpoint *createNetworkEndpoint(const QSettings* settings);
    static AbstractEndpoint *createProcessEndpoint(const QSettings* settings);
};

#endif // ENDPOINTFACTORY_H
