#include <QCoreApplication>

#include <QTcpServer>
#include <QTcpSocket>
#include <QCommandLineParser>

#include "multiplexer.h"
#include "roomprotocol.h"
#include "endpointfactory.h"


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QCommandLineParser parser;

    QCommandLineOption discoveryInterval(QStringList() << "a" << "auto-discover", "Endpoints auto discovery", "interval in s", QString::number(-1));
    parser.addOption(discoveryInterval);

    parser.process(a);

    const QString endpointsFilePath = QCoreApplication::applicationDirPath() + QString("/endpoints.ini");

    QVector<AbstractEndpoint*> fixedEndpoints = EndpointFactory::createEndpointsFromFile(endpointsFilePath);

    //Node related
    Multiplexer mux(QString("Remote Mux"), fixedEndpoints);
    QTcpServer *serv = new QTcpServer();

    QObject::connect(serv, &QTcpServer::newConnection, [&] () {
        QTcpSocket* socket = serv->nextPendingConnection();

        qDebug() << "New connection from" << socket->peerAddress();

        RoomLink* link = new RoomLink();
        link->setDevice(socket);
        socket->setParent(link);

        QObject::connect(link, &RoomLink::msgReceived, &mux, &Multiplexer::msgToEndpoint);
        QObject::connect(&mux, &Multiplexer::msgFromEndpoint, link, &RoomLink::transmitMsg);
        QObject::connect(socket, &QTcpSocket::disconnected, link, &RoomLink::deleteLater);
        QObject::connect(socket, &QTcpSocket::disconnected, [&] () {
            qDebug() << "Connection closed";
        });
    });

    quint16 port = ROOM_TCP_DEFAULT_PORT;
    if(!serv->listen( QHostAddress::Any, port))
    {
        qCritical() << "Failed to listen on port" << port;
        return -1;
    }
    else
    {
        qInfo() << "Listening on port" << serv->serverPort();
    }

    int discoveryIntervalInt = parser.value(discoveryInterval).toInt();

    mux.doEndpointsDiscovery();
    if(mux.setAutoDiscovery(discoveryIntervalInt * 1000))
    {
        qInfo() << "Auto discovery every" << discoveryIntervalInt << "seconds";
    }

    return a.exec();
}
