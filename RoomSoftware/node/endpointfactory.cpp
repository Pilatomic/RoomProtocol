#include "endpointfactory.h"
#include "networkendpoint.h"
#include "processendpoint.h"
#include "roomprotocol.h"
#include <QSettings>
#include <QtDebug>

const QString typeString("type");
const QString networkHostString("host");
const QString networtkPortString("port");
const QString processPathString("path");
const QString processArgsString("args");

const EndpointFactory::EndpointCreatorMap EndpointFactory::creatorMap =
        EndpointFactory::EndpointCreatorMap(
            {
                { "network"     , &EndpointFactory::createNetworkEndpoint   },
                { "process"     , &EndpointFactory::createProcessEndpoint   },
            }
);

QVector<AbstractEndpoint *> EndpointFactory::createEndpointsFromFile(const QString &filePath)
{
    QSettings s(filePath,QSettings::IniFormat);
    QVector<AbstractEndpoint*> createdEndpoints;

    if(s.allKeys().isEmpty())
    {
        qWarning() << "Error loading endpoints file :"
                   << filePath
                   << "No fixed endpoints will be created";
    }
    else
    {
        QStringList groups = s.childGroups();
        for(auto grpIt = groups.constBegin() ; grpIt != groups.constEnd() ; grpIt++)
        {
            s.beginGroup(*grpIt);
            AbstractEndpoint* endpoint = createEndpointFromSettings(&s);
            if(endpoint)
            {
                endpoint->setObjectName(*grpIt);
                createdEndpoints.append(endpoint);
            }
            s.endGroup();
        }
    }
    return createdEndpoints;
}

AbstractEndpoint *EndpointFactory::createEndpointFromSettings(const QSettings *settings)
{
    QString type = settings->value(typeString).toString();
    EndpointCreatorFunc creatorFunc = creatorMap.value(type, nullptr);
    return (creatorFunc == nullptr) ? nullptr : creatorFunc(settings);
}

AbstractEndpoint *EndpointFactory::createNetworkEndpoint(const QSettings *settings)
{
    QString host = settings->value(networkHostString).toString();
    quint16 port = quint16(settings->value(networtkPortString, ROOM_TCP_DEFAULT_PORT).toInt());
    qInfo() << "Creating network endpoint. host :" << host
            << "port :" << port;
    return new NetworkEndpoint(host,port);
}

AbstractEndpoint *EndpointFactory::createProcessEndpoint(const QSettings *settings)
{
    QString path = settings->value(processPathString).toString();
    QString args = settings->value(processArgsString).toString();
    qInfo() << "Creating process endpoint. path :" << path
            << "args :" << args;

    return new ProcessEndpoint(path, args);
}
